VERSION 5.00
Begin VB.Form frmnewuser 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   Caption         =   "New User"
   ClientHeight    =   2790
   ClientLeft      =   5220
   ClientTop       =   4635
   ClientWidth     =   4830
   LinkTopic       =   "Form1"
   ScaleHeight     =   2790
   ScaleWidth      =   4830
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   4815
      Begin VB.TextBox txtconfpassword 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1920
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   6
         Top             =   1440
         Width           =   2655
      End
      Begin VB.TextBox txtpassword 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1920
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   840
         Width           =   2655
      End
      Begin VB.TextBox txtnewusername 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   10
         TabIndex        =   4
         Top             =   240
         Width           =   2655
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Conf.Password:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   9
         Top             =   1440
         Width           =   1560
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "Password:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   1020
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "User Name:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1215
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H000080FF&
         BorderColor     =   &H000080FF&
         FillColor       =   &H000080FF&
         Height          =   495
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   2895
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H000080FF&
         BorderColor     =   &H000080FF&
         FillColor       =   &H000080FF&
         Height          =   495
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   2895
      End
      Begin VB.Shape Shape3 
         BackColor       =   &H000080FF&
         BorderColor     =   &H000080FF&
         FillColor       =   &H000080FF&
         Height          =   495
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   1320
         Width           =   2895
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   2160
      Width           =   4815
      Begin VB.CommandButton cmdok 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Ok"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   120
         Width           =   885
      End
      Begin VB.CommandButton cmdExit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Exit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   120
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmnewuser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public rsUser As New ADODB.Recordset
Private Sub cmdexit_Click()
Unload Me
End Sub

Private Sub cmdok_Click()
Dim StrSql As String
StrSql = "select * from Login"
If Trim(txtnewusername.Text) = "" Then
    MsgBox "Please Enter User Name ", vbInformation + vbOKOnly
    txtnewusername.SetFocus
    Exit Sub
End If
If Trim(txtpassword.Text) = "" Then
    MsgBox "Please Enter Password ", vbInformation + vbOKOnly
    txtpassword.SetFocus
    Exit Sub
End If

If Trim(txtconfpassword.Text) = "" Then
    MsgBox "Please Enter Confirm Password ", vbInformation + vbOKOnly
    txtnewusername.SetFocus
    Exit Sub
End If

If Trim(txtpassword.Text) <> Trim(txtconfpassword.Text) Then
    MsgBox "Please Check Confirm Pasasword", vbInformation + vbOKOnly
    txtconfpassword.Text = ""
    txtconfpassword.SetFocus
    Exit Sub
End If
With rsUser

    If .State Then .Close
    .Open StrSql, cnn, adOpenDynamic, adLockOptimistic
    .AddNew
    .Fields("Username") = txtnewusername.Text
    .Fields("Password") = txtpassword.Text
    .Update
    
    MsgBox "Record save Successfully", vbInformation + vbOKOnly
End With
 txtnewusername.Text = ""
 txtpassword.Text = ""
 txtconfpassword.Text = ""
 txtnewusername.SetFocus
End Sub


