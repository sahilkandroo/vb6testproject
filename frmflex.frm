VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmflexhospital 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Hospital Master"
   ClientHeight    =   6735
   ClientLeft      =   1665
   ClientTop       =   2460
   ClientWidth     =   8430
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   8430
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdnew 
      BackColor       =   &H00E0E0E0&
      Caption         =   "N&ew "
      Height          =   435
      Left            =   720
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "For new entry"
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2160
      TabIndex        =   1
      Top             =   240
      Width           =   5745
   End
   Begin MSFlexGridLib.MSFlexGrid msfHospital 
      CausesValidation=   0   'False
      Height          =   5445
      Left            =   360
      TabIndex        =   0
      Top             =   840
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   9604
      _Version        =   393216
      Rows            =   3
      Cols            =   3
      FixedCols       =   0
      BackColorFixed  =   -2147483638
      BackColorSel    =   16706266
      ForeColorSel    =   255
      BackColorBkg    =   16777215
      Redraw          =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmflexhospital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ctr As Integer

Private Sub cmdexit_Click()
Unload Me
End Sub

Private Sub cmdnew_Click()
frmhospitalmaster.Show vbModal
frmhospitalmaster.cmdclear.Visible = True
frmhospitalmaster.cmddelete.Visible = False
End Sub

Private Sub Form_Load()
DrawAndFillGridHospitalMaster
End Sub


Public Function DrawAndFillGridHospitalMaster()
Dim rsHospital As New ADODB.Recordset

    If rsHospital.State Then rsHospital.Close
    rsHospital.Open "HospitalMaster", cnn, adOpenDynamic, adLockOptimistic
    
    With msfHospital
        .Clear
        .ColWidth(0) = 1
        .ColWidth(1) = 5000
        .ColWidth(2) = 2500
                
        .TextMatrix(0, 0) = "Hospital ID"
        .TextMatrix(0, 1) = "Hospital Name"
        .TextMatrix(0, 2) = "Phone No"
        
        .Rows = rsHospital.RecordCount + 1
        
        For ctr = 1 To rsHospital.RecordCount
            .TextMatrix(ctr, 0) = rsHospital("PK_HospitalID")
            .TextMatrix(ctr, 1) = rsHospital("HospitalName")
            .TextMatrix(ctr, 2) = rsHospital("PhoneNo")
            rsHospital.MoveNext
        Next
    End With


End Function


Private Sub msfHospital_DblClick()
If msfHospital.Row = 0 Then Exit Sub
frmhospitalmaster.AssignHospData Val(msfHospital.TextMatrix(msfHospital.Row, 0))
frmhospitalmaster.Show vbModal
frmhospitalmaster.cmdclear.Visible = False
frmhospitalmaster.cmddelete.Visible = True
End Sub

Private Sub txtSearch_Change()

Dim i As Integer
For i = 1 To msfHospital.Rows - 1
    If InStr(1, Mid(msfHospital.TextMatrix(i, 1), 1, Len(txtSearch.Text)), txtSearch.Text, vbBinaryCompare) > 0 Then
        msfHospital.Row = i
        msfHospital.RowSel = i
        msfHospital.ColSel = 2
        'msfindividuallist.SetFocus
        'HighLightRow i
        Exit For
    End If
Next

End Sub
