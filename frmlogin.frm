VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form login 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   Caption         =   $"frmlogin.frx":0000
   ClientHeight    =   10230
   ClientLeft      =   105
   ClientTop       =   150
   ClientWidth     =   15240
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10230
   ScaleWidth      =   15240
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ProgressBar Bar3 
      Height          =   3015
      Left            =   10680
      TabIndex        =   10
      Top             =   5040
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   5318
      _Version        =   393216
      Appearance      =   0
      Max             =   31
      Orientation     =   1
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar Bar2 
      Height          =   3015
      Left            =   5160
      TabIndex        =   9
      Top             =   5040
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   5318
      _Version        =   393216
      Appearance      =   0
      Max             =   31
      Orientation     =   1
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar Bar1 
      Height          =   135
      Left            =   5400
      TabIndex        =   8
      Top             =   4920
      Width           =   5085
      _ExtentX        =   8969
      _ExtentY        =   238
      _Version        =   393216
      Appearance      =   0
      Max             =   31
      Scrolling       =   1
   End
   Begin MSComctlLib.ProgressBar Bar 
      Height          =   135
      Left            =   5400
      TabIndex        =   7
      Top             =   8040
      Width           =   5085
      _ExtentX        =   8969
      _ExtentY        =   238
      _Version        =   393216
      Appearance      =   0
      Max             =   31
      Scrolling       =   1
   End
   Begin VB.Timer tmr1 
      Interval        =   900
      Left            =   6000
      Top             =   6000
   End
   Begin VB.CommandButton cmdOk 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Ok"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6240
      MaskColor       =   &H00000000&
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   7080
      Width           =   1080
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8445
      MaskColor       =   &H00000000&
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   7080
      Width           =   1170
   End
   Begin VB.TextBox txtpass 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF8080&
      Height          =   465
      IMEMode         =   3  'DISABLE
      Left            =   7440
      MaxLength       =   10
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   6240
      Width           =   2655
   End
   Begin VB.TextBox txtuname 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   7440
      MaxLength       =   10
      TabIndex        =   0
      Top             =   5640
      Width           =   2655
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   4455
      Left            =   2400
      Picture         =   "frmlogin.frx":00AB
      Stretch         =   -1  'True
      Top             =   120
      Width           =   11415
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Open Your Heart to Donate Blood"
      BeginProperty Font 
         Name            =   "Lucida Handwriting"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   4560
      TabIndex        =   11
      Top             =   9840
      Width           =   7575
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00EFEADE&
      BorderWidth     =   2
      X1              =   4650
      X2              =   11115
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   5310
      X2              =   5310
      Y1              =   4560
      Y2              =   8520
   End
   Begin VB.Line Line8 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   10755
      X2              =   10755
      Y1              =   4560
      Y2              =   8520
   End
   Begin VB.Line Line7 
      BorderColor     =   &H00EFEADE&
      BorderWidth     =   2
      X1              =   4665
      X2              =   11115
      Y1              =   4905
      Y2              =   4920
   End
   Begin VB.Line Line6 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   10635
      X2              =   10635
      Y1              =   4560
      Y2              =   8520
   End
   Begin VB.Line Line5 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   4665
      X2              =   11115
      Y1              =   8040
      Y2              =   8025
   End
   Begin VB.Line Line4 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   5190
      X2              =   5190
      Y1              =   4560
      Y2              =   8520
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   4665
      X2              =   11115
      Y1              =   8160
      Y2              =   8160
   End
   Begin VB.Shape Shape5 
      BackColor       =   &H00A07247&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   3495
      Left            =   5040
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape6 
      BackColor       =   &H00A07247&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   3495
      Left            =   10515
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape8 
      BackColor       =   &H00A07247&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   375
      Left            =   5400
      Top             =   4800
      Width           =   5130
   End
   Begin VB.Shape Shape7 
      BackColor       =   &H00A07247&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   375
      Left            =   5415
      Top             =   7920
      Width           =   5115
   End
   Begin VB.Label lbl1 
      Alignment       =   2  'Center
      BackColor       =   &H00FF8080&
      BackStyle       =   0  'Transparent
      Caption         =   "30"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   7545
      TabIndex        =   4
      Top             =   6960
      Width           =   855
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H000000FF&
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Index           =   1
      Left            =   5880
      TabIndex        =   6
      Top             =   6375
      Width           =   1110
   End
   Begin VB.Label Label1 
      BackColor       =   &H000000FF&
      BackStyle       =   0  'Transparent
      Caption         =   "User Name"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   5880
      TabIndex        =   5
      Top             =   5640
      Width           =   1245
   End
End
Attribute VB_Name = "login"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdcancel_Click()
Dim can
can = MsgBox("ARE YOU SURE YOU WANT TO CANCEL", vbYesNo + vbQuestion, "Cancelation")
If can = vbYes Then
    Unload Me
    End
Else
txtuname.SetFocus
End If
End Sub


Private Sub cmdok_Click()
Dim count
Dim rs As New ADODB.Recordset
Dim sql As String
Dim quit
If txtuname.Text = "" Then
   MsgBox "Fill The User Name!!!", vbInformation, "User Name Missing "
   txtuname.Text = ""
   txtuname.SetFocus
   SendKeys "{home}" & "+{end}"
   
   Exit Sub
End If
If txtpass.Text = "" Then
   MsgBox "Please Enter Password!!!", vbInformation, "Password Missing"
   txtpass.Text = ""
   txtpass.SetFocus
   SendKeys "{home}" & "+{end}"
   Exit Sub
End If
If IsNumeric(Trim(txtuname.Text)) Then
     MsgBox "Please Enter Characters Only", vbInformation
     txtuname.Text = ""
     txtuname.SetFocus
Exit Sub
End If
If Trim(txtuname.Text) = "" Then
   MsgBox "Please Enter Correct Information", vbInformation + vbOKOnly
    txtuname.Text = ""
     txtuname.SetFocus
End If



On Error GoTo e1
Dim StrSql As String
    StrSql = "select * from Login where username='" & txtuname.Text & "' and password='" & txtpass.Text & "'"
    If dbrec.State = adStateOpen Then
        dbrec.Close
    End If
Module1.main

    dbrec.Open StrSql, dbcon, adOpenDynamic, adLockOptimistic
e1:
    If Err.Number = 3709 Then
    
     ElseIf Not dbrec.EOF Then
     USER_NAME = txtuname.Text
    MsgBox "!!! Login Done Successfully !!! " & vbCrLf & vbCrLf & "User Name:- " & USER_NAME & vbCrLf & vbCrLf & "Time :-" & Time, vbOKOnly + vbInformation, "login"
   
      MDIForm1.Show
  MDIForm1.StatusBar1.Panels(3) = USER_NAME
        Unload Me

    Else
    Call login
    End If
  dbcon.Close
  
 End Sub
        
    


Public Function login()
Dim can
 can = MsgBox("Wrong Username and Password!!", vbInformation, "login Failed")
If can = vbOK Then
txtuname.Text = ""
  txtpass.Text = ""
   txtuname.SetFocus
   Else

End If

End Function




Private Sub Form_Load()
   Counter = 0
    Me.Height = Screen.Height - 400
    Me.Width = (Screen.Width)
    Randomize
   txtpass.Text = ""
End Sub

Private Sub Timer1_Timer()
lblgal.ForeColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
End Sub

Private Sub txtpass_GotFocus()
    If txtuname.Text = "" Then
        MsgBox "User Name can not be Empty", vbOKOnly, "user missing"
        txtuname.SetFocus
    End If
End Sub
Private Sub tmr1_Timer()

    Line1.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line2.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line3.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line4.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line5.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line6.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line7.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    Line8.BorderColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)
    lbl1.ForeColor = RGB(Rnd * 255, Rnd * 255, Rnd * 255)

    lbl1.Caption = lbl1.Caption - 1
    Bar.Value = Bar.Value + 1
    Bar1.Value = Bar1.Value + 1
    Bar2.Value = Bar2.Value + 1
    Bar3.Value = Bar3.Value + 1
    If lbl1.Caption = 0 Then
        lbl1.Caption = 30
        Bar.Value = 1
        Bar1.Value = 1
        Bar2.Value = 1
        Bar3.Value = 1
        'tmr1.Interval = 0
    End If
    If lbl1.Caption = 30 Then
    MsgBox "System will Terminate Now !!!", vbOKOnly, "Time Ended!!!"
    End
    End If
End Sub

Private Sub txtuname_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32, 46
Case 65 To 90
Case 97 To 122
Case Else
KeyAscii = 0
End Select
End Sub
