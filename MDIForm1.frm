VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H00000000&
   Caption         =   $"MDIForm1.frx":0000
   ClientHeight    =   10710
   ClientLeft      =   360
   ClientTop       =   840
   ClientWidth     =   14115
   LinkTopic       =   "MDIForm1"
   Picture         =   "MDIForm1.frx":00A1
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   240
      Top             =   9720
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   10290
      Width           =   14115
      _ExtentX        =   24897
      _ExtentY        =   741
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   16
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   1129
            MinWidth        =   1129
            Picture         =   "MDIForm1.frx":C1C7
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   2
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "User Name:"
            TextSave        =   "User Name:"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   3087
            MinWidth        =   3087
            Text            =   "Waiting...            "
            TextSave        =   "Waiting...            "
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   1129
            MinWidth        =   1129
            Picture         =   "MDIForm1.frx":D845
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   2
            Object.Width           =   2117
            MinWidth        =   2117
            Text            =   "Time Log-in:"
            TextSave        =   "Time Log-in:"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   2117
            MinWidth        =   2117
            Text            =   "Waiting..."
            TextSave        =   "Waiting..."
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   1129
            MinWidth        =   1129
            Picture         =   "MDIForm1.frx":E833
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   1411
            MinWidth        =   1411
            Text            =   "Date:"
            TextSave        =   "Date:"
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   2117
            MinWidth        =   2117
            TextSave        =   "12/7/2008"
         EndProperty
         BeginProperty Panel10 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   2
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Total Form : "
            TextSave        =   "Total Form : "
         EndProperty
         BeginProperty Panel11 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
            Object.Width           =   1323
            MinWidth        =   1323
         EndProperty
         BeginProperty Panel12 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            Bevel           =   2
            Enabled         =   0   'False
            Object.Width           =   1235
            MinWidth        =   1235
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel13 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            Bevel           =   2
            Enabled         =   0   'False
            Object.Width           =   1235
            MinWidth        =   1235
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel14 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   4
            Alignment       =   1
            Bevel           =   2
            Enabled         =   0   'False
            Object.Width           =   1235
            MinWidth        =   1235
            TextSave        =   "SCRL"
         EndProperty
         BeginProperty Panel15 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   2
            Object.Width           =   1129
            MinWidth        =   1129
            Picture         =   "MDIForm1.frx":F965
         EndProperty
         BeginProperty Panel16 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuEmp 
      Caption         =   "Employee"
      Begin VB.Menu mnuNewUser 
         Caption         =   "New User"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuChangePassword 
         Caption         =   "Change Password"
      End
   End
   Begin VB.Menu mnugen 
      Caption         =   "General"
      Begin VB.Menu mnuDonorList 
         Caption         =   "Donor List"
      End
      Begin VB.Menu mnuhosplist 
         Caption         =   "Hospital Master"
      End
   End
   Begin VB.Menu mnublood 
      Caption         =   "Blood Details"
      Begin VB.Menu mnubloodgr 
         Caption         =   "BloodGroup"
      End
   End
   Begin VB.Menu mnutrans 
      Caption         =   "Transaction"
      Begin VB.Menu mnuTransactionList 
         Caption         =   "Transaction List"
      End
      Begin VB.Menu mnutransdet 
         Caption         =   "Transaction Details"
      End
   End
   Begin VB.Menu mnureport 
      Caption         =   "Reports"
      Begin VB.Menu mnuDonorListrpt 
         Caption         =   "Donor List Report"
      End
      Begin VB.Menu mnuTransaction 
         Caption         =   "Transaction"
         Begin VB.Menu mnuDonorTransaction 
            Caption         =   "Donor Transaction"
         End
         Begin VB.Menu mnuOutTrans 
            Caption         =   "Outsider Transaction "
         End
         Begin VB.Menu mnuHospTrans 
            Caption         =   "Hospital Transaction "
         End
      End
   End
   Begin VB.Menu mnucon 
      Caption         =   "Contacts"
      Begin VB.Menu mnuaddbook 
         Caption         =   "Address Book"
      End
      Begin VB.Menu mnuabout 
         Caption         =   "About Us"
      End
   End
   Begin VB.Menu mnuexit 
      Caption         =   "E&xit"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Load()
Call Connect
StatusBar1.Panels(6) = Time$


End Sub

Private Sub mnuabout_Click()
frmSplash.Show
End Sub

Private Sub mnuaddbook_Click()
frmflexdesclist.Show
End Sub

Private Sub mnubloodgr_Click()
frmflexbloodgr.Show
End Sub

Private Sub mnubloodinvent_Click()
frminventorylist.Show
End Sub

Private Sub mnuBloodInventory_Click()
rptBloodInventory.Show
End Sub

Private Sub mnuChangePassword_Click()
frmchangepass.Show
End Sub

Private Sub mnudonorlist_Click()
frmflexindividual.Show
End Sub

Private Sub mnuDonorListrpt_Click()
frmDonorList.Show
End Sub

Private Sub mnuDonorTransaction_Click()
frmdonortrans.Show
End Sub

Private Sub mnuexit_Click()
Unload Me
End

End Sub

Private Sub mnuhosplist_Click()
frmflexhospital.Show
End Sub

Private Sub mnuHospTrans_Click()
frmhosptrans.Show
End Sub

Private Sub mnuNewUser_Click()
frmnewuser.Show
End Sub

Private Sub mnuOutTrans_Click()
frmOutTrans.Show

End Sub

Private Sub mnutoc_Click()
frmflexdesclist.Show
End Sub

Private Sub mnuTransactionList_Click()
frmTransList.Show
End Sub

Private Sub mnutransdet_Click()
frmtransaction.Show
End Sub

Private Sub Timer1_Timer()
StatusBar1.Panels(16) = Time$
End Sub
