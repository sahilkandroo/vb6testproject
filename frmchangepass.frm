VERSION 5.00
Begin VB.Form frmchangepass 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Change Password"
   ClientHeight    =   3315
   ClientLeft      =   5715
   ClientTop       =   4335
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3315
   ScaleWidth      =   4620
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      TabIndex        =   9
      Top             =   2640
      Width           =   4575
      Begin VB.CommandButton cmdExit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Exit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   105
         Width           =   855
      End
      Begin VB.CommandButton cmdok 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Ok"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1275
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   105
         Width           =   885
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4575
      Begin VB.ComboBox cbousername 
         Appearance      =   0  'Flat
         BackColor       =   &H00DAFCFB&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   2040
         TabIndex        =   4
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox txtoldpass 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   8
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   840
         Width           =   2175
      End
      Begin VB.TextBox txtnewpass 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   8
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1440
         Width           =   2175
      End
      Begin VB.TextBox txtconfpass 
         Appearance      =   0  'Flat
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   8
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   720
         Width           =   2370
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   1320
         Width           =   2370
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   1920
         Width           =   2370
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   2370
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Caption         =   "&User Name:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Caption         =   "&Old Password:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   840
         Width           =   1440
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Caption         =   "New Password:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   1440
         Width           =   1530
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Caption         =   "Conf.Password:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   2040
         Width           =   1560
      End
   End
End
Attribute VB_Name = "frmchangepass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rs As New ADODB.Recordset
Public Function fillusername()
Dim i As Integer
    For i = 1 To rs.RecordCount
         cbousername.AddItem rs.Fields("username")
         rs.MoveNext
    Next
End Function

Private Sub cbousername_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case Else
        KeyAscii = 0
End Select
End Sub

Private Sub cmdexit_Click()
Unload Me
End Sub

Private Sub cmdok_Click()

If Trim(cbousername.Text = "") Then
    MsgBox "Please Select The User Name", vbInformation + vbOKOnly
    cbousername.SetFocus
    Exit Sub
End If

Dim rsPass As New ADODB.Recordset
Dim sqlstr As String
With rs
     If .State Then .Close
    Dim StrSql As String
        StrSql = "select * from Login where username='" & cbousername.Text & "' and password='" & txtoldpass.Text & "'"
       If .State Then .Close
       .Open StrSql, cnn, adOpenDynamic, adLockOptimistic
         If txtnewpass.Text <> txtconfpass.Text Then
                MsgBox "Please Check Confirm Pasasword", vbInformation + vbOKOnly
                txtconfpass.Text = ""
                txtconfpass.SetFocus
                 Exit Sub
         End If
         
         If OldNotNew = True Then
         Exit Sub
         Else
         End If
         
        If .RecordCount > 0 Then
        .Update
            .Fields("password") = txtnewpass.Text
        .Update
            MsgBox "Password Upadted", vbInformation + vbOKOnly
        Else
             MsgBox "Please Enter Old Password Correctly"
             txtoldpass.Text = ""
            txtoldpass.SetFocus
            Exit Sub
        End If
 End With
 
 cbousername.Text = ""
 txtconfpass.Text = ""
 txtnewpass.Text = ""
 txtoldpass.Text = ""
 cbousername.SetFocus

End Sub
Function OldNotNew() As Boolean
Dim rs As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From Login where password='" & txtnewpass.Text & "'"
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic

If rs.RecordCount > 0 Then
    'If rs("password") = txtnewpass.Text Then
        MsgBox "Your Old Password Is Same As The New Password " & vbNewLine & "Please Change Your Password For Security ", vbInformation + vbOKOnly
        txtnewpass.Text = ""
        txtnewpass.SetFocus
        OldNotNew = True
        Exit Function
    'End If
End If
    

End Function


Private Sub CommandButton1_Click()

End Sub

Private Sub Form_Load()
With rs
      If .State Then .Close
      .ActiveConnection = cnn
      .CursorLocation = adUseClient
      .LockType = adLockBatchOptimistic
      .CursorType = adOpenDynamic
      .Open "login"
End With
Call fillusername
End Sub



