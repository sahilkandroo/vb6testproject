VERSION 5.00
Begin VB.Form frmbloodinventorydetail 
   BackColor       =   &H00C0FFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BloodGroup Details"
   ClientHeight    =   2370
   ClientLeft      =   285
   ClientTop       =   675
   ClientWidth     =   3240
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   3240
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3015
      Begin VB.Label lblexpiredate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   1440
         TabIndex        =   7
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label lblacceptdate 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   1440
         TabIndex        =   6
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label4 
         BackColor       =   &H000080FF&
         Height          =   120
         Left            =   0
         TabIndex        =   5
         Top             =   720
         Width           =   3060
      End
      Begin VB.Label lblbloodgrname 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   375
         Left            =   1440
         TabIndex        =   4
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         Caption         =   " BloodGroup :"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Expiry Date :"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Accept Date :"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1080
         Width           =   2055
      End
   End
End
Attribute VB_Name = "frmbloodinventorydetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Bloodgr As Integer

Dim Date1 As Integer
Public Function Assignbloodname(bloodid As Integer)
Dim rs As New ADODB.Recordset
Dim rs1 As New ADODB.Recordset
Dim Strsql As String
Dim Strsql1 As String
Strsql = "Select * From BloodGroup Where PK_BloodGroupID=" & bloodid
'If rsblood.State Then rsblood.Close
rs.Open Strsql, cnn, adOpenDynamic, adLockOptimistic
If rs.RecordCount > 0 Then
    'IsMod = True
    Bloodgr = bloodid
    
    lblbloodgrname.Caption = rs("BloodGroupName")
    'lblacceptdate.Caption = rs("Date")
End If
Strsql1 = "Select * From IndividualMaster Where PK_DonorID=" & Donid
'If rsblood.State Then rsblood.Close
rs1.Open Strsql1, cnn, adOpenDynamic, adLockOptimistic
If rs1.RecordCount > 0 Then
    'IsMod = True
     lblacceptdate.Caption = rs1("Date")
End If
'Call CalcExpiryDate
End Function

Public Function CalcExpiryDate()
'Dim ITime As Date
'Dim OTime As Date
'Dim DiffTime
'ITime = Format(txtInTime.Text, "HH:MM AM/PM")
'OTime = Format(txtOutTime.Text, "HH:MM AM/PM")
'DiffTime = Format(ITime - OTime, "HH:MM")
Dim ITime As Date
Dim OTime As Date
Dim DiffTime
ITime = Format(lblacceptdate.Caption, "MM:DD:YY")
'OTime = Format(txtOutTime.Text, "HH:MM AM/PM")
'DiffTime = Format(ITime - OTime, "HH:MM")
lblexpiredate.Caption = ITime
End Function

