VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmOutTrans 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Outsider Transaction"
   ClientHeight    =   1530
   ClientLeft      =   5295
   ClientTop       =   4665
   ClientWidth     =   5475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1530
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Width           =   5415
      Begin VB.CommandButton cmdPrint 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Print"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   570
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   3480
         TabIndex        =   1
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   73269251
         CurrentDate     =   39097
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   960
         TabIndex        =   2
         Top             =   120
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   73269251
         CurrentDate     =   39097
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   3000
         TabIndex        =   3
         Top             =   120
         Width           =   375
      End
   End
End
Attribute VB_Name = "frmOutTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdPrint_Click()
If DTPicker1.Value > DTPicker2.Value Then
MsgBox "Please check Selected Dates", vbInformation + vbOKOnly
Exit Sub
End If
If DTPicker2.Value > Now Then
MsgBox "select 2nd Date as Todays Date"
Exit Sub
End If
OutsiderTransReport
End Sub


Function OutsiderTransReport()
Dim rs As New ADODB.Recordset
Dim StrSql As String
StrSql = " SELECT Trans.TransactionDate,Trans.Amount, OutSider.Name, BloodGroup.BloodGroupName, TransactionDetails.NOB, Trans.TypeOfReciepent" & _
         " FROM ((Trans INNER JOIN TransactionDetails ON Trans.PK_TransactionID = TransactionDetails.FK_TransID) INNER JOIN OutSider ON Trans.FK_RecepientID = OutSider.PK_OutSiderID) INNER JOIN BloodGroup ON TransactionDetails.FK_BloodGroupID = BloodGroup.PK_BloodGroupID" & _
        " WHERE (((Trans.TransactionDate) Between #" & Format(DTPicker1.Value, "dd-MMM-yyyy") & " # And # " & Format(DTPicker2.Value, "dd-MMM-yyyy") & " #) AND ((Trans.TypeOfReciepent)='2'))"


 
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rs.RecordCount < 1 Then
    MsgBox "Record not present", vbInformation
    Exit Function
Else

Set OutTrans.DataSource = rs


With OutTrans
    .Sections("Section2").Controls.Item("lblFrom").Caption = Format(DTPicker1.Value, "dd-MMM-yyyy")
    .Sections("section2").Controls.Item("lblTo").Caption = Format(DTPicker2.Value, "dd-MMM-yyyy")
End With


With OutTrans.Sections("Section1").Controls
    .Item("txtDate").DataField = "TransactionDate"
    .Item("txtName").DataField = "Name"
    .Item("txtBloodGroup").DataField = "BloodGroupName"
    .Item("txtNOB").DataField = "NOB"
    .Item("txtAmount").DataField = "Amount"
    
End With

With OutTrans.Sections("Section5").Controls
    .Item("funTotal").DataField = "Amount"
End With

OutTrans.Refresh
OutTrans.Show
End If
End Function



Private Sub Form_Load()
DTPicker2.Value = Now
End Sub
