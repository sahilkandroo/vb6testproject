VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmindividual 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Individual Details"
   ClientHeight    =   5580
   ClientLeft      =   4395
   ClientTop       =   3225
   ClientWidth     =   7500
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   7500
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   240
      TabIndex        =   52
      Top             =   4890
      Width           =   7155
      Begin VB.CommandButton cmddeleteasd 
         BackColor       =   &H00E0E0E0&
         Caption         =   "D&elete"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         Style           =   1  'Graphical
         TabIndex        =   73
         ToolTipText     =   "Delete"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdsave 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Sa&ve"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "Update the DataBase"
         Top             =   158
         Width           =   1125
      End
      Begin VB.CommandButton cmdclear 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cl&ear"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4080
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Clear the sceen"
         Top             =   165
         Width           =   1125
      End
      Begin VB.CommandButton cmdexit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Unload the current window"
         Top             =   165
         Width           =   1125
      End
      Begin VB.Shape Shape17 
         BorderColor     =   &H000080FF&
         Height          =   450
         Left            =   2160
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   1335
      End
      Begin VB.Shape Shape9 
         BorderColor     =   &H000080FF&
         Height          =   450
         Left            =   5760
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1335
      End
      Begin VB.Shape Shape10 
         BorderColor     =   &H000080FF&
         Height          =   450
         Left            =   4005
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1335
      End
      Begin VB.Shape Shape11 
         BorderColor     =   &H000080FF&
         Height          =   450
         Left            =   120
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1335
      End
   End
   Begin VB.Frame frmpassword 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   1200
      TabIndex        =   68
      Top             =   1560
      Visible         =   0   'False
      Width           =   4455
      Begin VB.CommandButton cmdright 
         Default         =   -1  'True
         Height          =   495
         Left            =   1440
         Picture         =   "frmindividual.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   1560
         Width           =   735
      End
      Begin VB.CommandButton cmdwrong 
         Height          =   495
         Left            =   2400
         Picture         =   "frmindividual.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   1560
         Width           =   735
      End
      Begin VB.TextBox txtdelpass 
         Alignment       =   2  'Center
         BackColor       =   &H00FEEADA&
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   720
         PasswordChar    =   "*"
         TabIndex        =   69
         Top             =   960
         Width           =   2895
      End
      Begin VB.Label Label30 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "!!!!Please Enter Password For Deleting !!!!"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   70
         Top             =   360
         Width           =   3975
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5595
      Left            =   30
      TabIndex        =   16
      Top             =   15
      Width           =   7425
      _ExtentX        =   13097
      _ExtentY        =   9869
      _Version        =   393216
      TabHeight       =   520
      BackColor       =   16777215
      ForeColor       =   33023
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Donor Details"
      TabPicture(0)   =   "frmindividual.frx":1994
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Health Information"
      TabPicture(1)   =   "frmindividual.frx":19B0
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Donation Details"
      TabPicture(2)   =   "frmindividual.frx":19CC
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame7"
      Tab(2).Control(1)=   "msfDonate"
      Tab(2).Control(2)=   "Frame6"
      Tab(2).ControlCount=   3
      Begin VB.Frame Frame6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1035
         Left            =   -74880
         TabIndex        =   57
         Top             =   435
         Width           =   7140
         Begin MSComCtl2.DTPicker dtdondate 
            Height          =   375
            Left            =   5400
            TabIndex        =   65
            Top             =   120
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   661
            _Version        =   393216
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   71303169
            CurrentDate     =   39518
         End
         Begin VB.CommandButton cmdSaveData 
            BackColor       =   &H00E0E0E0&
            Caption         =   "&Save"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6000
            Style           =   1  'Graphical
            TabIndex        =   18
            ToolTipText     =   "Update the DataBase"
            Top             =   615
            Width           =   990
         End
         Begin VB.TextBox txtNOB 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3360
            MaxLength       =   2
            TabIndex        =   17
            Text            =   "1"
            ToolTipText     =   "Enter No Of Bags"
            Top             =   120
            Width           =   495
         End
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   330
            Left            =   120
            TabIndex        =   63
            ToolTipText     =   "Select Date"
            Top             =   480
            Visible         =   0   'False
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd-MMM-yyyy"
            Format          =   71303171
            CurrentDate     =   39048
         End
         Begin VB.Label lbldel 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   120
            TabIndex        =   64
            Top             =   480
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label Label31 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Donate Date"
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   3960
            TabIndex        =   62
            Top             =   120
            Width           =   1290
         End
         Begin VB.Label lblBG 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000040C0&
            Height          =   210
            Left            =   1395
            TabIndex        =   61
            Top             =   135
            Width           =   540
         End
         Begin VB.Label Label20 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "No Of Bags"
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   2160
            TabIndex        =   60
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label Label9 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "BloodGroup: "
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   120
            TabIndex        =   59
            Top             =   120
            Width           =   1350
         End
      End
      Begin MSFlexGridLib.MSFlexGrid msfDonate 
         Height          =   3195
         Left            =   -74895
         TabIndex        =   56
         Top             =   1605
         Width           =   7200
         _ExtentX        =   12700
         _ExtentY        =   5636
         _Version        =   393216
         Rows            =   10
         Cols            =   3
         FixedCols       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   5385
         Left            =   -74985
         TabIndex        =   35
         Top             =   345
         Width           =   7425
         Begin VB.Frame Frame5 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   1.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   4395
            Left            =   90
            TabIndex        =   36
            Top             =   60
            Width           =   7215
            Begin VB.TextBox txttemp 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2400
               MaxLength       =   5
               TabIndex        =   67
               ToolTipText     =   "EnterBody Temprature"
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtweight 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2370
               MaxLength       =   3
               TabIndex        =   66
               ToolTipText     =   "Enter Body Weight"
               Top             =   255
               Width           =   735
            End
            Begin VB.TextBox txtbagwt 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2415
               MaxLength       =   3
               TabIndex        =   14
               ToolTipText     =   "Enter Weight Of Bag"
               Top             =   3870
               Width           =   720
            End
            Begin VB.TextBox txtglobin 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2400
               MaxLength       =   2
               TabIndex        =   13
               ToolTipText     =   "Enter HB"
               Top             =   3135
               Width           =   735
            End
            Begin VB.TextBox txtpressure 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2400
               MaxLength       =   3
               TabIndex        =   12
               ToolTipText     =   "Enter Blood Pressure"
               Top             =   2400
               Width           =   735
            End
            Begin VB.TextBox txtpulse 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2400
               MaxLength       =   3
               TabIndex        =   11
               ToolTipText     =   "Enter Pulse Rate"
               Top             =   1710
               Width           =   735
            End
            Begin VB.Label Label28 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Wt of Bag"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   165
               TabIndex        =   55
               Top             =   3885
               Width           =   1020
            End
            Begin VB.Shape Shape18 
               BorderColor     =   &H000080FF&
               Height          =   495
               Left            =   2295
               Shape           =   4  'Rounded Rectangle
               Top             =   3750
               Width           =   3720
            End
            Begin VB.Label Label29 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               Caption         =   "ml"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   3270
               TabIndex        =   54
               Top             =   3885
               Width           =   375
            End
            Begin VB.Label Label27 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Upto 350 ml"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4695
               TabIndex        =   53
               Top             =   3885
               Width           =   1170
            End
            Begin VB.Label Label26 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "50 To 100"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4815
               TabIndex        =   51
               Top             =   1725
               Width           =   960
            End
            Begin VB.Label Label25 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "70 To 180"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4800
               TabIndex        =   50
               Top             =   2415
               Width           =   960
            End
            Begin VB.Label Label24 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "12 To 16"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4920
               TabIndex        =   49
               Top             =   3150
               Width           =   840
            End
            Begin VB.Label Label23 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "beats/min"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3240
               TabIndex        =   48
               Top             =   1725
               Width           =   960
            End
            Begin VB.Label Label22 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "mm HG"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3240
               TabIndex        =   47
               Top             =   2415
               Width           =   780
            End
            Begin VB.Label Label21 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Gms/100 ml"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3240
               TabIndex        =   46
               Top             =   3150
               Width           =   1185
            End
            Begin VB.Label Label18 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "36.5 To 37.5"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4560
               TabIndex        =   45
               Top             =   990
               Width           =   1200
            End
            Begin VB.Label Label17 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   " *C"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3240
               TabIndex        =   44
               Top             =   1035
               Width           =   345
            End
            Begin VB.Label Label16 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "50 or More"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   4710
               TabIndex        =   43
               Top             =   285
               Width           =   1050
            End
            Begin VB.Label Label15 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   " KG"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3240
               TabIndex        =   42
               Top             =   285
               Width           =   420
            End
            Begin VB.Shape Shape16 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   2280
               Shape           =   4  'Rounded Rectangle
               Top             =   3030
               Width           =   3720
            End
            Begin VB.Label Label14 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Haemoglobin"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   150
               TabIndex        =   41
               Top             =   3165
               Width           =   1320
            End
            Begin VB.Shape Shape15 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   2280
               Shape           =   4  'Rounded Rectangle
               Top             =   2310
               Width           =   3720
            End
            Begin VB.Label Label13 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Blood Pressure"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   135
               TabIndex        =   40
               Top             =   2445
               Width           =   1530
            End
            Begin VB.Label Label12 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Pulse-Rate"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   135
               TabIndex        =   39
               Top             =   1725
               Width           =   1110
            End
            Begin VB.Shape Shape14 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   2280
               Shape           =   4  'Rounded Rectangle
               Top             =   1605
               Width           =   3720
            End
            Begin VB.Shape Shape13 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   2265
               Shape           =   4  'Rounded Rectangle
               Top             =   885
               Width           =   3720
            End
            Begin VB.Label Label11 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Temprature"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   135
               TabIndex        =   38
               Top             =   1005
               Width           =   1185
            End
            Begin VB.Label Label19 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Body Weight"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   135
               TabIndex        =   37
               Top             =   285
               Width           =   1305
            End
            Begin VB.Shape Shape21 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   2280
               Shape           =   4  'Rounded Rectangle
               Top             =   165
               Width           =   3720
            End
         End
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   5400
         Left            =   60
         TabIndex        =   10
         Top             =   360
         Width           =   7395
         Begin VB.Frame Frame3 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   1.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   4395
            Left            =   60
            TabIndex        =   15
            Top             =   60
            Width           =   7215
            Begin VB.ComboBox cbobloodgroup 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1410
               TabIndex        =   9
               ToolTipText     =   "Select your Bloodgroup"
               Top             =   3750
               Width           =   2205
            End
            Begin VB.ComboBox cboid 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   3060
               TabIndex        =   24
               Top             =   3765
               Visible         =   0   'False
               Width           =   705
            End
            Begin VB.TextBox txtmob 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   4830
               MaxLength       =   10
               TabIndex        =   8
               ToolTipText     =   "Enter mobile  No."
               Top             =   3090
               Width           =   2175
            End
            Begin VB.PictureBox Picture2 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   495
               Left            =   4080
               Picture         =   "frmindividual.frx":19E8
               ScaleHeight     =   495
               ScaleWidth      =   375
               TabIndex        =   23
               Top             =   3045
               Width           =   375
            End
            Begin VB.TextBox txtphone 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   1440
               MaxLength       =   8
               TabIndex        =   7
               ToolTipText     =   "Enter landline  phone No."
               Top             =   3120
               Width           =   2175
            End
            Begin VB.PictureBox Picture1 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   495
               Left            =   180
               Picture         =   "frmindividual.frx":222E
               ScaleHeight     =   495
               ScaleWidth      =   615
               TabIndex        =   22
               Top             =   3090
               Width           =   615
            End
            Begin VB.OptionButton optmale 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               Caption         =   "Male"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   6120
               TabIndex        =   6
               ToolTipText     =   "Select your Sex"
               Top             =   2520
               Width           =   960
            End
            Begin VB.OptionButton optfemale 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               Caption         =   "Female"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   4920
               TabIndex        =   5
               ToolTipText     =   "Select your sex"
               Top             =   2505
               Width           =   1110
            End
            Begin VB.TextBox txtage 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1440
               MaxLength       =   2
               TabIndex        =   4
               ToolTipText     =   "Enter age"
               Top             =   2535
               Width           =   1455
            End
            Begin VB.TextBox txtpincode 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   5160
               MaxLength       =   6
               TabIndex        =   3
               ToolTipText     =   "Enter pincode "
               Top             =   1965
               Width           =   1815
            End
            Begin VB.TextBox txtcity 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2160
               TabIndex        =   2
               ToolTipText     =   "Enter name of city"
               Top             =   1965
               Width           =   1815
            End
            Begin VB.TextBox txtaddress 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   885
               Left            =   1425
               TabIndex        =   1
               ToolTipText     =   "Enter adress"
               Top             =   795
               Width           =   5535
            End
            Begin VB.TextBox txtname 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1425
               TabIndex        =   0
               ToolTipText     =   "Enter name"
               Top             =   240
               Width           =   5535
            End
            Begin MSComCtl2.DTPicker dtdateIndividual 
               Height          =   330
               Left            =   4800
               TabIndex        =   25
               Top             =   3735
               Width           =   1530
               _ExtentX        =   2699
               _ExtentY        =   582
               _Version        =   393216
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MMM-yyyy"
               Format          =   71303171
               CurrentDate     =   39048
            End
            Begin VB.Shape Shape12 
               BorderColor     =   &H000080FF&
               Height          =   450
               Left            =   4725
               Shape           =   4  'Rounded Rectangle
               Top             =   3660
               Width           =   1680
            End
            Begin VB.Label Label10 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Date"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   4020
               TabIndex        =   34
               Top             =   3810
               Width           =   495
            End
            Begin VB.Shape Shape8 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   3675
               Width           =   2355
            End
            Begin VB.Label Label8 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "BloodGroup"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   135
               TabIndex        =   33
               Top             =   3825
               Width           =   1215
            End
            Begin VB.Shape Shape7 
               BorderColor     =   &H000080FF&
               Height          =   480
               Left            =   4710
               Shape           =   4  'Rounded Rectangle
               Top             =   3000
               Width           =   2400
            End
            Begin VB.Shape Shape6 
               BorderColor     =   &H000080FF&
               Height          =   480
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   3037
               Width           =   2340
            End
            Begin VB.Shape Shape5 
               BorderColor     =   &H000080FF&
               Height          =   390
               Left            =   4860
               Shape           =   4  'Rounded Rectangle
               Top             =   2445
               Width           =   2235
            End
            Begin VB.Label Label7 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Gender"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3960
               TabIndex        =   32
               Top             =   2535
               Width           =   750
            End
            Begin VB.Label Label6 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Yrs"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3000
               TabIndex        =   31
               Top             =   2550
               Width           =   345
            End
            Begin VB.Shape Shape3 
               BorderColor     =   &H000080FF&
               Height          =   480
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   2445
               Width           =   2100
            End
            Begin VB.Label Label5 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Age"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   270
               TabIndex        =   30
               Top             =   2610
               Width           =   405
            End
            Begin VB.Shape Shape2 
               BorderColor     =   &H000080FF&
               Height          =   495
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   1860
               Width           =   5760
            End
            Begin VB.Label Label4 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "   Pincode"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   4080
               TabIndex        =   29
               Top             =   1980
               Width           =   960
            End
            Begin VB.Label Label3 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "   City"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1440
               TabIndex        =   28
               Top             =   1980
               Width           =   600
            End
            Begin VB.Shape Shape1 
               BorderColor     =   &H000080FF&
               Height          =   1095
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   690
               Width           =   5760
            End
            Begin VB.Label Label2 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Address"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   270
               TabIndex        =   27
               Top             =   765
               Width           =   825
            End
            Begin VB.Shape Shape4 
               BorderColor     =   &H000080FF&
               Height          =   465
               Left            =   1320
               Shape           =   4  'Rounded Rectangle
               Top             =   150
               Width           =   5760
            End
            Begin VB.Label Label1 
               Appearance      =   0  'Flat
               AutoSize        =   -1  'True
               BackColor       =   &H80000005&
               Caption         =   "Name"
               BeginProperty Font 
                  Name            =   "Times New Roman"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   270
               TabIndex        =   26
               Top             =   225
               Width           =   600
            End
         End
      End
      Begin VB.Frame Frame7 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   5430
         Left            =   -74970
         TabIndex        =   58
         Top             =   360
         Width           =   7335
      End
   End
End
Attribute VB_Name = "frmindividual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public IsMod As Boolean
Public Donorid As Integer

Private Sub cbobloodgroup_Change()
cbobloodgroup_Click
End Sub

Private Sub cbobloodgroup_Click()
cboid.ListIndex = cbobloodgroup.ListIndex
lblBG.Caption = cbobloodgroup.Text
End Sub

Private Sub cbobloodgroup_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 13
Case Else
KeyAscii = 0
End Select
End Sub

Private Sub cboid_Change()
cboid_Click
End Sub

Private Sub cboid_Click()
cbobloodgroup.ListIndex = cboid.ListIndex
End Sub

Private Sub cboid_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 13
Case Else
KeyAscii = 0
End Select
End Sub

Private Sub cmdclear_Click()
Call Clear(Me)
End Sub

Private Sub cmddel_Click(Index As Integer)
frmpassword.Visible = True
txtdelpass.SetFocus
txtdelpass.Text = ""
End Sub

Private Sub cmddelete_Click(Index As Integer)
frmpassword.Visible = True
txtdelpass.SetFocus
txtdelpass.Text = ""
End Sub

Private Sub cmddeleteasd_Click()
frmpassword.Visible = True
txtdelpass.SetFocus
txtdelpass.Text = ""
End Sub

Private Sub cmdexit_Click()
Unload Me
End Sub

Function ValidateIndividual() As Boolean
If Trim(txtname.Text) = "" Then
    MsgBox "Enter Name", vbInformation
    SSTab1.Tab = 0
    txtname.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtAddress.Text) = "" Then
    MsgBox "Enter address", vbInformation
    SSTab1.Tab = 0
    txtAddress.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtcity.Text) = "" Then
    MsgBox "Enter City name", vbInformation
    SSTab1.Tab = 0
    txtcity.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtpincode.Text) = "" Then
    MsgBox "Enter pincode", vbInformation
    SSTab1.Tab = 0
    txtpincode.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtage.Text) = "" Then
    MsgBox "Enter age", vbInformation
    SSTab1.Tab = 0
    txtage.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtphone.Text) = "" Then
    MsgBox "Enter phone No.", vbInformation
    SSTab1.Tab = 0
    txtphone.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtmob.Text) = "" Then
    MsgBox "Enter mobile No.", vbInformation
    SSTab1.Tab = 0
    txtmob.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(cbobloodgroup.Text) = "" Then
    MsgBox "Select Bloodgroup", vbInformation
    SSTab1.Tab = 0
    cbobloodgroup.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtweight.Text) = "" Then
    MsgBox "Enter weight of Donor", vbInformation
    SSTab1.Tab = 1
    txtweight.SetFocus
    SSTab1.Tab = 1
    ValidateIndividual = False
    Exit Function
End If

If Trim(txttemp.Text) = "" Then
    MsgBox "Enter temprature of Donor", vbInformation
    SSTab1.Tab = 1
    txttemp.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtpulse.Text) = "" Then
    MsgBox "Enter pulse-rate of Donor", vbInformation
    SSTab1.Tab = 1
    txtpulse.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtpressure.Text) = "" Then
    MsgBox "Enter blood pressure of Donor", vbInformation
    SSTab1.Tab = 1
    txtpressure.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtglobin.Text) = "" Then
    MsgBox "Enter heamoglobin of Donor", vbInformation
    SSTab1.Tab = 1
    txtglobin.SetFocus
    ValidateIndividual = False
    Exit Function
End If

If Trim(txtbagwt.Text) = "" Then
    MsgBox "Enter weight of bag of Donor", vbInformation
    SSTab1.Tab = 1
    txtbagwt.SetFocus
    ValidateIndividual = False
    Exit Function
End If

msfDonate.Row = 1
If msfDonate.TextMatrix(msfDonate.Row, 1) = "" Then
    MsgBox "Select Blood Donation details"
    SSTab1.Tab = 2
    cmdSaveData.SetFocus
    ValidateIndividual = False
    Exit Function
End If


    ValidateIndividual = True

End Function

Private Sub cmdright_Click()
If txtdelpass.Text = "jeevandan" Then
If MsgBox("Do You Want To Delete?", vbQuestion + vbYesNo) = vbYes Then
        Dim rs1 As New ADODB.Recordset
            With rs1
                If .State Then .Close
                Dim StrSql As String
                StrSql = "delete * from IndividualMaster where PK_DonorID=" & lbldel
                .Open StrSql, cnn, adOpenDynamic, adLockBatchOptimistic
               
                MsgBox "Record Deleted", vbInformation + vbOKOnly
                frmpassword.Visible = False
                Call Clear(Me)
                frmflexindividual.DrawAndFillGridIndividual
                   
                Call Refresh
                              
        End With
Else
        Exit Sub
End If
Else
MsgBox "Sorry You Don't have Permission for it!!", vbOKOnly, "Unauthorised Person"
frmpassword.Visible = False
End If

End Sub

Private Sub cmdsave_Click()
If ValidateIndividual = False Then Exit Sub
Dim gen As String
Dim rsIndividual As New ADODB.Recordset

If ValidateIndividual = False Then Exit Sub

If IsMod Then
    rsIndividual.Open "Select * From IndividualMaster Where PK_DonorID=" & Donorid, cnn, adOpenDynamic, adLockOptimistic
    rsIndividual.Update
Else
    rsIndividual.Open "Select * From IndividualMaster Where 1=2", cnn, adOpenDynamic, adLockOptimistic
    rsIndividual.AddNew
End If
    'If rsIndividual.State Then rsIndividual.Close
        rsIndividual("DonorName") = txtname.Text
        rsIndividual("Add") = txtAddress.Text
        If Val(txtage.Text) >= 18 And Val(txtage.Text) <= 55 Then
            rsIndividual("Age") = txtage.Text
        Else
            MsgBox "Invalid Age", vbInformation
            txtage.Text = ""
            txtage.SetFocus
            Exit Sub
        End If
        If optfemale.Value = True Then
            rsIndividual("Gender") = "Female"
       Else
            rsIndividual("Gender") = "Male"
        End If
        If Val(txtNOB.Text) >= 3 Then
    MsgBox "Donar cannot Donate more Than 2 Bags", vbInformation
    txtNOB.Text = ""
    txtNOB.SetFocus
    Else
    
                
                
        rsIndividual("Telephone") = txtphone.Text
        rsIndividual("Mobile") = txtmob.Text
        rsIndividual("Date") = Format(dtdateIndividual.Value, "dd-MMM-yyyy")
        rsIndividual("FK_BloodGroupID") = cboid.Text
        rsIndividual("City") = txtcity.Text
        rsIndividual("Pincode") = txtpincode.Text
        rsIndividual("BodyWeight") = txtweight.Text
        rsIndividual("Temprature") = txtpressure.Text
        rsIndividual("Pulse") = txtpulse.Text
        rsIndividual("BP") = txtpressure.Text
        rsIndividual("HB") = txtglobin.Text
        rsIndividual("WtOfBag") = txtbagwt.Text
    rsIndividual.Update
    
If IsMod = False Then SaveGrid Val(rsIndividual("PK_DonorID"))
MsgBox "Record save successfully", vbInformation
frmflexindividual.DrawAndFillGridIndividual


Call Clear(Me)
Unload Me
End If
End Sub


Private Sub cmdSaveData_Click()
Dim d As Date
Dim dm As Integer
Dim m As Integer



msfDonate.Row = 1
If IsMod Then
    While msfDonate.TextMatrix(msfDonate.Row, 2) <> ""
        DTPicker1.Value = msfDonate.TextMatrix(msfDonate.Row, 2)
        msfDonate.Row = msfDonate.Row + 1
    Wend
    d = Format(dtdondate.Value, "dd-MMM-yyyy")
    If d = DTPicker1.Value Then
        MsgBox "Check Donation Date", vbInformation
        Exit Sub
    End If
    
    If dtdondate.Value > d Then
        dm = Format(dtdondate.Value, "mm")
        m = Format(d, "mm")
        If Val(dm - d) <= 3 Then
            MsgBox "Donor can not donote blood!," & vbNewLine & "Since 9 Months are not completed from last Blood donation!!", vbOKOnly + vbCritical, "9 Month's are not completed"
            Exit Sub
        End If
            
    End If
    If donor_date = True Then
        SaveDonateData Donorid, Val(txtNOB.Text), Format(dtdondate.Value, "dd-MMM-yyyy")
        FillDonateGrid Donorid
    Else
        Exit Sub
    End If
    'cmdsave.Enabled = True
Else
    If donor_date = True Then
        msfDonate.TextMatrix(1, 1) = txtNOB.Text
        msfDonate.TextMatrix(1, 2) = Format(dtdondate.Value, "dd-MMM-yyyy")
    'cmdsave.Enabled = True
    Else
        Exit Sub
    End If
End If
End Sub
Public Function donor_date() As Boolean
If Trim(txtNOB.Text) = "" Or Trim(txtNOB.Text) = 0 Then
    MsgBox "Enter No Of Blood Bags donated", vbInformation
    SSTab1.Tab = 2
    txtNOB.SetFocus
    donor_date = False
    Exit Function
End If
donor_date = True
End Function

Private Sub Command1_Click()
End Sub

Private Sub cmdwrong_Click()
frmpassword.Visible = False

End Sub

Private Sub Form_Load()
 dtdateIndividual.Value = Now
' dtdondate.Value = Now
'Call Connect
Call FillBloodgroup
optfemale.Value = True

BuildGrid
End Sub

Public Function FillBloodgroup()
Dim rsbloodgroup As New ADODB.Recordset
With rsbloodgroup
    If rsbloodgroup.State Then rsbloodgroup.Close
    .Open "BloodGroup", cnn, adOpenDynamic, adLockOptimistic
    Dim i As Integer
    cbobloodgroup.Clear
    cboid.Clear
    For i = 1 To .RecordCount
    cbobloodgroup.AddItem .Fields("BloodGroupName")
    cboid.AddItem .Fields("PK_BloodGroupID")
    .MoveNext
    Next
End With
End Function



Private Sub txtAddress_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32
Case 65 To 90, 97 To 122, 44 To 57
Case Else
KeyAscii = 0
End Select

End Sub


Private Sub txtage_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 48 To 57
Case 8
Case Else
KeyAscii = 0
End Select
End Sub

Private Sub txtbagwt_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtcity_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 65 To 90, 97 To 122
Case 8
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtglobin_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtmob_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub
Private Sub txtname_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32, 46
Case 65 To 90
Case 97 To 122
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtNOB_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtphone_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8, 45
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtpincode_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub



Public Function AssignData(Donid As Integer)
Dim rsInd As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From IndividualMaster Where PK_DonorID=" & Donid
rsInd.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
FillBloodgroup
If rsInd.RecordCount > 0 Then
    IsMod = True
    
    Donorid = Donid
    FillDonateGrid Donorid
    lbldel.Caption = rsInd("PK_DonorID")
    txtname.Text = rsInd("DonorName")
    txtAddress.Text = rsInd("Add")
    txtage.Text = rsInd("Age")
    txtcity.Text = rsInd("City")
    txtpincode.Text = rsInd("Pincode")
    txtmob.Text = rsInd("Mobile")
    txtphone.Text = rsInd("Telephone")
    dtdondate.Value = rsInd("Date")
    If rsInd("Gender") = "Female" Then
        optfemale.Value = True
    Else
        optmale.Value = True
    End If
    dtdateIndividual.Value = Now
    cboid.Text = rsInd("FK_BloodGroupID")
    cbobloodgroup.ListIndex = cboid.ListIndex
    lblBG.Caption = cbobloodgroup.Text
    txtweight.Text = rsInd("BodyWeight")
    txttemp.Text = rsInd("Temprature")
    txtpulse.Text = rsInd("Pulse")
    txtpressure.Text = rsInd("BP")
    txtglobin.Text = rsInd("HB")
    txtbagwt.Text = rsInd("WtOfBag")
    Dim rsbld As New ADODB.Recordset
    Dim bld As Integer
    bld = rsInd("FK_BloodGroupID")
    Dim Strsqlbd As String
    Strsqlbd = "Select * From BloodGroup Where PK_BloodGroupID=" & bld
    rsbld.Open Strsqlbd, cnn, adOpenDynamic, adLockOptimistic
    
    cbobloodgroup.Text = rsbld("BloodGroupName")
  cmdsave.Enabled = False
    cmddeleteasd.Enabled = True
    
    
End If
End Function
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
IsMod = False
End Sub



Sub BuildGrid()
With msfDonate
    .Clear
    .Cols = 3
    .Rows = 10
    .TextMatrix(0, 1) = "No.Of Bags"
    '.TextMatrix(0, 2) = "No.Of Bags"
    .TextMatrix(0, 2) = "Donate Date"
    .ColWidth(0) = 1
    .ColWidth(1) = 2500
    '.ColWidth(2) = 1500
    .ColWidth(2) = 2500

End With

End Sub


Function SaveDonateData(IndID As Integer, NOB As Integer, DDate As Date) As Integer
Dim rsDonateTR As New ADODB.Recordset
Dim StrSql As String
StrSql = "INSERT INTO DonateTransaction(FK_IndividualID,NoOfBags,DonateDate) " & _
         " VALUES(" & IndID & "," & NOB & ",#" & DDate & "#)"
         
rsDonateTR.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
UpdateInventory IndID, NOB

End Function


Function SaveGrid(IndID As Integer)
'If IsMod = False Then Exit Function
Dim i As Integer
For i = 1 To msfDonate.Rows
    If msfDonate.TextMatrix(i, 1) = "" Then Exit For
    
    SaveDonateData IndID, msfDonate.TextMatrix(i, 1), msfDonate.TextMatrix(i, 2)
Next
End Function




Function FillDonateGrid(IndID As Integer)
Dim d As Date
Dim rsInd As New ADODB.Recordset
Dim StrSql As String, i As Integer
StrSql = "Select * From DonateTransaction Where FK_IndividualID=" & IndID
rsInd.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rsInd.RecordCount > 0 Then
    For i = 1 To rsInd.RecordCount
        msfDonate.TextMatrix(i, 0) = rsInd("PK_DonateTransactionID")
        msfDonate.TextMatrix(i, 1) = rsInd("NoOfBags")
        msfDonate.TextMatrix(i, 2) = Format(rsInd("DonateDate"), "dd-MMM-yyyy")
        d = Format(rsInd("DonateDate"), "dd-MMM-yyyy")
        rsInd.MoveNext
    Next
End If
End Function


Function UpdateInventory(IndID As Integer, Qty As Integer)
Dim rsInd As New ADODB.Recordset
Dim sqlBG As String, StrSql As String, i As Integer
'sqlBG = "SELECT BloodGroup.PK_BloodGroupID FROM DonateTransaction " & _
'         " INNER JOIN (IndividualMaster INNER JOIN BloodGroup ON  " & _
'         " IndividualMaster.FK_BloodGroupID = BloodGroup.PK_BloodGroupID) ON " & _
'         " DonateTransaction.FK_IndividualID = IndividualMaster.PK_DonorID " & _
'         " GROUP BY BloodGroup.PK_BloodGroupID " & _
'         " HAVING (((Min(DonateTransaction.FK_IndividualID))=" & IndID & "))"

sqlBG = "SELECT BloodGroup.PK_BloodGroupID FROM IndividualMaster " & _
       " INNER JOIN BloodGroup ON IndividualMaster.FK_BloodGroupID = BloodGroup.PK_BloodGroupID " & _
       " WHERE (((IndividualMaster.PK_DonorID)=" & IndID & "))"

StrSql = "UPDATE BloodGroup Set AvailableQty=AvailableQty+" & Qty & " Where PK_BloodGroupID=(" & sqlBG & ")"
rsInd.Open StrSql, cnn, adOpenDynamic, adLockOptimistic

End Function

Private Sub txtpressure_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtpulse_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtselpass_Change()

End Sub



Private Sub txttemp_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtweight_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub
