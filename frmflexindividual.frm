VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmflexindividual 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Individual list"
   ClientHeight    =   8490
   ClientLeft      =   2160
   ClientTop       =   1965
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdFind 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "&Find"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7680
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   360
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      TabIndex        =   2
      Top             =   360
      Width           =   5745
   End
   Begin VB.CommandButton cmdnew 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "New "
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   240
      Width           =   915
   End
   Begin MSFlexGridLib.MSFlexGrid msfindividuallist 
      CausesValidation=   0   'False
      Height          =   5565
      Left            =   360
      TabIndex        =   0
      Top             =   960
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   9816
      _Version        =   393216
      Rows            =   8
      Cols            =   8
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   14737632
      BackColorSel    =   16706266
      ForeColorSel    =   255
      BackColorBkg    =   16777215
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmflexindividual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rsIndividual As New ADODB.Recordset
Dim ctr As Integer
Private Sub cmdExit_Click()
Unload Me
End Sub

Private Sub cmdFind_Click()

Dim i As Integer
For i = 1 To msfindividuallist.Rows - 1
    If InStr(1, Mid(msfindividuallist.TextMatrix(i, 1), 1, Len(txtSearch.Text)), txtSearch.Text, vbBinaryCompare) > 0 Then
        msfindividuallist.Row = i
        msfindividuallist.RowSel = i
        msfindividuallist.ColSel = 7
        'HighLightRow i
        Exit For
    End If
Next

End Sub

Private Sub cmdnew_Click()
frmindividual.Show vbModal
'cmddeleteasd.enable = False
'frmindividual.cmdsave.Enabled = False
End Sub
Public Function DrawAndFillGridIndividual()
    If rsIndividual.State Then rsIndividual.Close
    rsIndividual.Open "select * from IndividualMaster", cnn, adOpenDynamic, adLockOptimistic
    
    With msfindividuallist
        .Clear
        .ColWidth(0) = 1
        .ColWidth(1) = 1500
        .ColWidth(2) = 1500
        .ColWidth(3) = 1500
        .ColWidth(4) = 1500
        .ColWidth(5) = 1500
        .ColWidth(6) = 1500
        .ColWidth(7) = 1500
        
            
               
        .TextMatrix(0, 0) = "Donor_ID"
        .TextMatrix(0, 1) = "Name"
        .TextMatrix(0, 2) = "Age"
        .TextMatrix(0, 3) = "Gender"
        .TextMatrix(0, 4) = "Telephone No."
        .TextMatrix(0, 5) = "Mobile No."
        .TextMatrix(0, 6) = "Bloodgroup"
        .TextMatrix(0, 7) = "Date"
        
        
        .Rows = rsIndividual.RecordCount + 1
        
        For ctr = 1 To rsIndividual.RecordCount
            .TextMatrix(ctr, 0) = rsIndividual("PK_DonorID")
            .TextMatrix(ctr, 1) = rsIndividual("DonorName")
            .TextMatrix(ctr, 2) = rsIndividual("Age")
            .TextMatrix(ctr, 3) = rsIndividual("Gender")
            .TextMatrix(ctr, 4) = rsIndividual("Telephone")
            .TextMatrix(ctr, 5) = rsIndividual("Mobile")
            .TextMatrix(ctr, 6) = rsIndividual("FK_BloodGroupID")
            .TextMatrix(ctr, 7) = rsIndividual("Date")
            
            rsIndividual.MoveNext
        Next
    End With


End Function


Private Sub Form_Load()
Call DrawAndFillGridIndividual
End Sub

Private Sub msfindividuallist_Click()
HighLightRow msfindividuallist.Row
End Sub

Private Sub msfindividuallist_DblClick()
If msfindividuallist.Row = 0 Then Exit Sub
frmindividual.AssignData Val(msfindividuallist.TextMatrix(msfindividuallist.Row, 0))
frmindividual.Show vbModal
End Sub

Private Sub msfindividuallist_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then msfindividuallist_DblClick
End Sub

Private Sub msfindividuallist_RowColChange()
'HighLightRow msfindividuallist.Row
End Sub


Sub HighLightRow(rw As Integer)
''Dim i As Integer, j As Integer
''Dim rw As MSFlexGridLib.FocusRectSettings
''
''For j = 1 To msfindividuallist.Rows - 1
''    For i = 1 To msfindividuallist.Cols - 1
''        msfindividuallist.Row = j
''        msfindividuallist.Col = i
''            If j = rw Then
''                msfindividuallist.CellBackColor = &H400000
''                msfindividuallist.CellForeColor = vbWhite
''            Else
''                msfindividuallist.CellBackColor = vbWhite
''                msfindividuallist.CellForeColor = vbBlack
''            End If
''    Next
''Next
End Sub

Private Sub txtSearch_Change()

Dim i As Integer
For i = 1 To msfindividuallist.Rows - 1
    If InStr(1, Mid(msfindividuallist.TextMatrix(i, 1), 1, Len(txtSearch.Text)), txtSearch.Text, vbBinaryCompare) > 0 Then
        msfindividuallist.Row = i
        msfindividuallist.RowSel = i
        msfindividuallist.ColSel = 7
        'msfindividuallist.SetFocus
        'HighLightRow i
        Exit For
    End If
Next

End Sub

Private Sub txtSearch_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
Case 27
    txtSearch.Text = ""
Case 13
    msfindividuallist.SetFocus
End Select
End Sub
