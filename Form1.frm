VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTransList 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Transaction List"
   ClientHeight    =   8160
   ClientLeft      =   2565
   ClientTop       =   1455
   ClientWidth     =   11310
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   11310
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   2985
   End
   Begin MSFlexGridLib.MSFlexGrid msfTransList 
      CausesValidation=   0   'False
      Height          =   7005
      Left            =   360
      TabIndex        =   0
      Top             =   720
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   12356
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   14737632
      BackColorSel    =   16706266
      ForeColorSel    =   255
      BackColorBkg    =   16777215
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   150
      Width           =   735
   End
End
Attribute VB_Name = "frmTransList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Call DrawAndFill
End Sub
Public Function DrawAndFill()
Dim ctr As Integer
Dim rsTrans As New ADODB.Recordset
Dim strsqltrans As String
strsqltrans = "Select Distinct * From Trans"
    If rsTrans.State Then rsTrans.Close
    rsTrans.Open strsqltrans, cnn, adOpenDynamic, adLockOptimistic
    
    With msfTransList
        .Clear
        .Cols = 5
        .ColWidth(0) = 1
        .ColWidth(1) = 5000
        .ColWidth(2) = 1
        .ColWidth(3) = 1
        .ColWidth(4) = 5000
        
        .TextMatrix(0, 0) = "Transaction ID"
        .TextMatrix(0, 1) = "Transaction No"
        .TextMatrix(0, 2) = "Type Of Reciepent"
        .TextMatrix(0, 3) = "Recepient ID"
        .TextMatrix(0, 4) = "Date"
        
        .Rows = rsTrans.RecordCount + 1
        
        For ctr = 1 To rsTrans.RecordCount
            .TextMatrix(ctr, 0) = rsTrans("PK_TransactionID")
            .TextMatrix(ctr, 1) = rsTrans("TransactionNo")
            .TextMatrix(ctr, 2) = rsTrans("TypeOfReciepent")
            .TextMatrix(ctr, 3) = rsTrans("FK_RecepientID")
            .TextMatrix(ctr, 4) = rsTrans("TransactionDate")
            rsTrans.MoveNext
        Next
    End With
End Function

Private Sub msfTransList_DblClick()
'Dim transid As Integer
If msfTransList.Row = 0 Then Exit Sub
Call RecepName
'transid = msfTransList.TextMatrix(msfTransList.Row, 0)
frmtransaction.TransData Val(msfTransList.TextMatrix(msfTransList.Row, 0))
frmtransaction.cmdsavetrans.Enabled = False
frmtransaction.cmdoutsiderdetails.Enabled = False
frmtransaction.cmdOkData.Enabled = False
frmtransaction.cmdclr.Enabled = False
frmtransaction.cmdprint.Enabled = True
frmtransaction.Show
End Sub
Public Function RecepName()
Dim TOR As Integer
TOR = msfTransList.TextMatrix(msfTransList.Row, 2)
If TOR = 1 Then
    Call donname
    frmtransaction.Show
End If
If TOR = 2 Then
    Call outsidername
    frmtransaction.Show
End If
If TOR = 3 Then
    Call hospname
    frmtransaction.Show
End If

End Function

Public Function donname()
frmtransaction.txtOutsider.Visible = True
frmtransaction.Frame3.Enabled = False
Dim did As Integer
did = msfTransList.TextMatrix(msfTransList.Row, 3)
Dim rsname As New ADODB.Recordset
Dim strsqlodonname As String
If rsname.State Then rsname.Close
 strsqlodonname = "Select DonorName From IndividualMaster where PK_DonorID=" & did
rsname.Open strsqlodonname, cnn, adOpenDynamic, adLockOptimistic
    frmtransaction.txtOutsider.Text = rsname("DonorName")
End Function


Public Function outsidername()
frmtransaction.optOutsider.Value = True
frmtransaction.Frame3.Enabled = False
frmtransaction.txtOutsider.Visible = True
Dim did As Integer
did = msfTransList.TextMatrix(msfTransList.Row, 3)
Dim rsname As New ADODB.Recordset
Dim strsqlodonname As String
If rsname.State Then rsname.Close
 strsqlodonname = "Select * From OutSider where PK_OutSiderID=" & did
rsname.Open strsqlodonname, cnn, adOpenDynamic, adLockOptimistic
    frmtransaction.txtOutsider.Text = rsname("Name")
    frmtransaction.txtAddress.Text = rsname("Address")
    frmtransaction.txtOutsiderPhone.Text = rsname("PhoneNo")
    frmtransaction.txtouthosp.Text = rsname("HospName")
End Function


Public Function hospname()
frmtransaction.optHospital.Value = True
frmtransaction.Frame3.Enabled = False
frmtransaction.txtOutsider.Visible = True

Dim did As Integer
did = msfTransList.TextMatrix(msfTransList.Row, 3)
Dim rsname As New ADODB.Recordset
Dim strsqlodonname As String
If rsname.State Then rsname.Close
 strsqlodonname = "Select HospitalName From HospitalMaster where PK_HospitalID=" & did
rsname.Open strsqlodonname, cnn, adOpenDynamic, adLockOptimistic
    frmtransaction.txtOutsider.Text = rsname("HospitalName")
End Function

Private Sub txtSearch_Change()

Dim i As Integer
For i = 1 To msfTransList.Rows - 1
    If InStr(1, Mid(msfTransList.TextMatrix(i, 1), 1, Len(txtSearch.Text)), txtSearch.Text, vbBinaryCompare) > 0 Then
        msfTransList.Row = i
        msfTransList.RowSel = i
        msfTransList.ColSel = 4
        'msfindividuallist.SetFocus
        'HighLightRow i
        Exit For
    End If
Next

End Sub
