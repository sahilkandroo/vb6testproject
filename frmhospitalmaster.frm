VERSION 5.00
Begin VB.Form frmhospitalmaster 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hospital Details"
   ClientHeight    =   4740
   ClientLeft      =   390
   ClientTop       =   1800
   ClientWidth     =   8460
   FillColor       =   &H00FFFFFF&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   8460
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   75
      TabIndex        =   12
      Top             =   3750
      Width           =   8355
      Begin VB.CommandButton cmdsave 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Save"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Update the DataBase"
         Top             =   285
         Width           =   1335
      End
      Begin VB.CommandButton cmdclear 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cl&ear"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Clear the sceen"
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdexit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6720
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Unload the current window"
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmddelete 
         BackColor       =   &H00E0E0E0&
         Caption         =   "D&elete"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Delete Record"
         Top             =   270
         Width           =   1335
      End
      Begin VB.Shape Shape7 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   6600
         Shape           =   4  'Rounded Rectangle
         Top             =   150
         Width           =   1575
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   4950
         Shape           =   4  'Rounded Rectangle
         Top             =   150
         Width           =   1575
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   240
         Shape           =   4  'Rounded Rectangle
         Top             =   165
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   3615
      Left            =   45
      TabIndex        =   0
      Top             =   15
      Width           =   8355
      Begin VB.TextBox txtcity 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2655
         MaxLength       =   15
         TabIndex        =   3
         ToolTipText     =   "Enter name of city"
         Top             =   2355
         Width           =   1815
      End
      Begin VB.TextBox txtpincode 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5625
         MaxLength       =   10
         TabIndex        =   4
         ToolTipText     =   "Enter pincode "
         Top             =   2355
         Width           =   1815
      End
      Begin VB.TextBox txthospitalname 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1890
         MultiLine       =   -1  'True
         TabIndex        =   1
         ToolTipText     =   "Enter name"
         Top             =   510
         Width           =   6240
      End
      Begin VB.TextBox txtphoneno 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         MaxLength       =   8
         TabIndex        =   5
         ToolTipText     =   "Enter phone No."
         Top             =   2955
         Width           =   2775
      End
      Begin VB.TextBox txtadd 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   870
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   2
         ToolTipText     =   "Address of hospital"
         Top             =   1140
         Width           =   6180
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "   City"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   1920
         TabIndex        =   15
         Top             =   2355
         Width           =   615
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "   Pincode"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   4560
         TabIndex        =   14
         Top             =   2355
         Width           =   975
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   2235
         Width           =   5760
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H000080FF&
         Height          =   555
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   2835
         Width           =   3000
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H000080FF&
         Height          =   1095
         Left            =   1785
         Shape           =   4  'Rounded Rectangle
         Top             =   1020
         Width           =   6450
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   1800
         Shape           =   4  'Rounded Rectangle
         Top             =   405
         Width           =   6420
      End
      Begin VB.Label lblphone 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Phone No :"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   180
         TabIndex        =   11
         Top             =   2940
         Width           =   1110
      End
      Begin VB.Label lbladd 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Address :"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   165
         TabIndex        =   10
         Top             =   1245
         Width           =   960
      End
      Begin VB.Label lblhospitalname 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Hospital Name :"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   165
         TabIndex        =   9
         Top             =   465
         Width           =   1635
      End
   End
   Begin VB.OLE OLE1 
      Height          =   1095
      Left            =   420
      TabIndex        =   13
      Top             =   2190
      Width           =   3015
   End
End
Attribute VB_Name = "frmhospitalmaster"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim IsMod As Boolean
Dim hospitalid As Integer
Private Sub cmdclear_Click()
Call Clear(Me)
End Sub

Private Sub cmddelete_Click()
If MsgBox("Do You Want To Delete?", vbQuestion + vbYesNo) = vbYes Then
        Dim rs1 As New ADODB.Recordset
            With rs1
                If .State Then .Close
                Dim StrSql As String
                StrSql = "delete * from HospitalMaster where PK_HospitalID=" & hospitalid
                .Open StrSql, cnn, adOpenDynamic, adLockBatchOptimistic
                MsgBox "Record Deleted", vbInformation + vbOKOnly
         
        End With
Else
        Exit Sub
End If
       'DrawAndFillBloodgr
        'Call ItemRefresh
        frmflexhospital.DrawAndFillGridHospitalMaster
        Unload Me

End Sub

Private Sub cmdexit_Click()
Unload Me
End Sub

Private Sub cmdsave_Click()
If ValidateHospital = False Then Exit Sub
Dim rshospdet As New ADODB.Recordset
If IsMod Then
    rshospdet.Open "Select * From HospitalMaster Where PK_HospitalID=" & hospitalid, cnn, adOpenDynamic, adLockOptimistic
    rshospdet.Update
Else
    rshospdet.Open "Select * From HospitalMaster Where PK_HospitalID=" & hospitalid, cnn, adOpenDynamic, adLockOptimistic
    rshospdet.AddNew
End If
        
        rshospdet("HospitalName") = txthospitalname.Text
        rshospdet("Address") = txtadd.Text
        rshospdet("City") = txtcity.Text
        rshospdet("Pincode") = txtpincode.Text
        rshospdet("PhoneNo") = txtphoneno.Text
rshospdet.Update

MsgBox "Record is saved successfully", vbInformation
frmflexhospital.DrawAndFillGridHospitalMaster
Unload Me
End Sub



Function ValidateHospital() As Boolean
If Trim(txthospitalname.Text) = "" Then
    MsgBox "Enter Hospital Name", vbInformation
    txthospitalname.SetFocus
    ValidateHospital = False
    Exit Function
End If

If Trim(txtadd.Text) = "" Then
    MsgBox "Enter Hospital address", vbInformation
    txtadd.SetFocus
    ValidateHospital = False
    Exit Function
End If

If Trim(txtcity.Text) = "" Then
    MsgBox "Enter City name", vbInformation
    txtcity.SetFocus
    ValidateHospital = False
    Exit Function
End If

If Trim(txtpincode.Text) = "" Then
    MsgBox "Enter Pincode No.", vbInformation
    txtpincode.SetFocus
    ValidateHospital = False
    Exit Function
End If


If Trim(txtphoneno.Text) = "" Then
    MsgBox "Enter Hospital phone No.", vbInformation
    txtphoneno.SetFocus
    ValidateHospital = False
    Exit Function
End If
    ValidateHospital = True

End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
IsMod = False
End Sub

Private Sub txtadd_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 8
Case 32
Case 65 To 90, 97 To 122, 44 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtcity_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 65 To 90, 97 To 122
Case 8, 32
Case Else
KeyAscii = 0
End Select
End Sub

Private Sub txthospitalname_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 8
Case 32, 46
Case 65 To 90
Case 97 To 122
Case Else
KeyAscii = 0
End Select
End Sub

Private Sub txtphoneno_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtpincode_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select
End Sub


Public Function AssignHospData(hospid As Integer)
Dim rshosp As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From HospitalMaster Where PK_HospitalID=" & hospid
If rshosp.State Then rshosp.Close
rshosp.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rshosp.RecordCount > 0 Then
    IsMod = True
    hospitalid = hospid
    
    txthospitalname.Text = rshosp("HospitalName")
    txtadd.Text = rshosp("Address")
    txtphoneno.Text = rshosp("PhoneNo")
    txtcity.Text = rshosp("City")
    txtpincode.Text = rshosp("Pincode")
End If
End Function
