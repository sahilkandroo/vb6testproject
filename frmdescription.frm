VERSION 5.00
Begin VB.Form frmdescription 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADDRESS-BOOK"
   ClientHeight    =   4515
   ClientLeft      =   6135
   ClientTop       =   4320
   ClientWidth     =   6330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4515
   ScaleWidth      =   6330
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   60
      TabIndex        =   6
      Top             =   3510
      Width           =   6195
      Begin VB.CommandButton cmdsave 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Save"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Update the DataBase"
         Top             =   285
         Width           =   975
      End
      Begin VB.CommandButton cmdclear 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cl&ear"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Clear the sceen"
         Top             =   270
         Width           =   975
      End
      Begin VB.CommandButton cmdexit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4920
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Unload the current window"
         Top             =   270
         Width           =   975
      End
      Begin VB.CommandButton cmddelete 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Delete"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Clear the sceen"
         Top             =   270
         Width           =   975
      End
      Begin VB.Shape Shape7 
         BorderColor     =   &H000080FF&
         Height          =   585
         Left            =   4800
         Shape           =   4  'Rounded Rectangle
         Top             =   150
         Width           =   1215
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H000080FF&
         Height          =   585
         Left            =   2550
         Shape           =   4  'Rounded Rectangle
         Top             =   150
         Width           =   1215
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   120
         Shape           =   4  'Rounded Rectangle
         Top             =   165
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   3450
      Left            =   60
      TabIndex        =   0
      Top             =   -45
      Width           =   6195
      Begin VB.TextBox txtphone 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         MaxLength       =   8
         TabIndex        =   10
         ToolTipText     =   "Enter Phone No."
         Top             =   2850
         Width           =   2775
      End
      Begin VB.TextBox txtadd 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   1680
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         ToolTipText     =   "Enter Address"
         Top             =   1605
         Width           =   4215
      End
      Begin VB.TextBox txtdescription 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   1
         ToolTipText     =   "Enter Blood-Bank name"
         Top             =   840
         Width           =   4215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   "Contact No"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   12
         Top             =   2880
         Width           =   1335
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H000080FF&
         Height          =   480
         Left            =   1560
         Shape           =   4  'Rounded Rectangle
         Top             =   2760
         Width           =   2985
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H000080FF&
         Height          =   1095
         Left            =   1560
         Shape           =   4  'Rounded Rectangle
         Top             =   1440
         Width           =   4440
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   " Address:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   1560
         Width           =   960
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H000080FF&
         Height          =   375
         Left            =   1560
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   3960
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   "  Jeevandan Blood Bank"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         Left            =   1800
         TabIndex        =   7
         Top             =   120
         Width           =   3270
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   1560
         Shape           =   4  'Rounded Rectangle
         Top             =   675
         Width           =   4440
      End
      Begin VB.Label lblbloodgr 
         AutoSize        =   -1  'True
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   "   Name:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   180
         TabIndex        =   5
         Top             =   855
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmdescription"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim IsMod As Boolean
Dim descriptionid As Integer
Private Sub cmdclear_Click()
Call Clear(Me)
'txtdescription.Text = ""
End Sub

Private Sub cmddelete_Click()
If MsgBox("Do You Want To Delete?", vbQuestion + vbYesNo) = vbYes Then
        Dim rs1 As New ADODB.Recordset
            With rs1
                If .State Then .Close
                Dim StrSql As String
                StrSql = "delete * from TypeOfContact where PK_TOCID=" & descriptionid
                .Open StrSql, cnn, adOpenDynamic, adLockBatchOptimistic
                MsgBox "Record Deleted", vbInformation + vbOKOnly
         
        End With
Else
        Exit Sub
End If
       'DrawAndFillBloodgr
        'Call ItemRefresh
        frmflexdesclist.DrawAndFillDesc
        Unload Me

End Sub

Private Sub cmdExit_Click()
Unload Me
End Sub

Private Sub cmdsave_Click()
If Validatedesc = False Then Exit Sub
Dim rsdiscription As New ADODB.Recordset

If IsMod Then
    rsdiscription.Open "Select * From TypeOfContact Where PK_TOCID=" & descriptionid, cnn, adOpenDynamic, adLockOptimistic
    rsdiscription.Update
Else
    rsdiscription.Open "Select * From TypeOfContact Where PK_TOCID=" & descriptionid, cnn, adOpenDynamic, adLockOptimistic
    rsdiscription.AddNew
End If

    rsdiscription("Name") = txtdescription.Text
    rsdiscription("Address") = txtadd.Text
    rsdiscription("PhoneNo") = txtphone.Text
        
rsdiscription.Update
MsgBox "Record save successfully", vbInformation
frmflexdesclist.DrawAndFillDesc
Unload Me

End Sub

Private Sub Form_Load()
'Call Connect

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
IsMod = False
End Sub

Private Sub txtadd_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32
Case 65 To 90, 97 To 122, 44 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtdescription_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 97 To 122
Case 65 To 90
Case 8
Case 32
Case Else
KeyAscii = 0
End Select
End Sub

Public Function Assigndesc(descid As Integer)
Dim rsdesc As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From TypeOfContact Where PK_TOCID=" & descid
'If rsdesc.Open Then rsdesc.Close
rsdesc.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rsdesc.RecordCount > 0 Then
    IsMod = True
    descriptionid = descid
    
    txtdescription.Text = rsdesc("Name")
    txtadd.Text = rsdesc("Address")
    txtphone.Text = rsdesc("PhoneNO")
End If
End Function

Public Function Validatedesc() As Boolean
If Trim(txtdescription.Text) = "" Then
    MsgBox "Enter Description", vbInformation
    txtdescription.SetFocus
    Validatedesc = False
    Exit Function
End If

If Trim(txtadd.Text) = "" Then
    MsgBox "Enter Address", vbInformation
    txtadd.SetFocus
    Validatedesc = False
    Exit Function
End If

If Trim(txtphone.Text) = "" Then
    MsgBox "Enter Phone No.", vbInformation
    txtphone.SetFocus
    Validatedesc = False
    Exit Function
End If


Validatedesc = True
End Function

Private Sub txtphone_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub
