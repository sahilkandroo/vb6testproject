VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frminventorylist 
   BackColor       =   &H00C0FFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inventory List"
   ClientHeight    =   6480
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6225
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6480
   ScaleWidth      =   6225
   Begin MSFlexGridLib.MSFlexGrid msfallbloodgrlist 
      Height          =   5370
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5505
      _ExtentX        =   9710
      _ExtentY        =   9472
      _Version        =   393216
      FixedCols       =   0
   End
End
Attribute VB_Name = "frminventorylist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdlist_Click()
msfallbloodgrlist.Visible = True
End Sub

Private Sub cmdstock_Click()
frmbloodstock.Show
End Sub

Public Function DrawAndFillInventoryList()
Dim ctr As Integer
Dim rsinven As New ADODB.Recordset

    If rsinven.State Then rsinven.Close
    rsinven.Open "IndividualMaster", cnn, adOpenDynamic, adLockOptimistic
    
    With msfallbloodgrlist
        .Clear
        .ColWidth(0) = 500
        .ColWidth(1) = 1500
        
        .TextMatrix(0, 0) = "No."
        .TextMatrix(0, 1) = "BloodGroupID"
        
        .Rows = rsinven.RecordCount + 1
        
        For ctr = 1 To rsinven.RecordCount
            .TextMatrix(ctr, 0) = rsinven("PK_DonorID")
            .TextMatrix(ctr, 1) = rsinven("FK_BloodGroupID")
            rsinven.MoveNext
        Next
    End With

End Function

Private Sub Form_Load()
Call DrawAndFillInventoryList
End Sub

Private Sub msfallbloodgrlist_DblClick()
If msfallbloodgrlist.Row = 0 Then Exit Sub
Donid = (msfallbloodgrlist.TextMatrix(msfallbloodgrlist.Row, 0))
frmbloodinventorydetail.Assignbloodname Val(msfallbloodgrlist.TextMatrix(msfallbloodgrlist.Row, 1))

frmbloodinventorydetail.Show
End Sub
