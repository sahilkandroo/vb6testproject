VERSION 5.00
Begin VB.Form frmbloodgroup 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Blood Group Details"
   ClientHeight    =   2685
   ClientLeft      =   4635
   ClientTop       =   1455
   ClientWidth     =   5280
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2685
   ScaleWidth      =   5280
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1515
      Left            =   105
      TabIndex        =   5
      Top             =   60
      Width           =   5115
      Begin VB.TextBox txtbloodgroup 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         MaxLength       =   10
         TabIndex        =   1
         ToolTipText     =   "Enter Blood Group"
         Top             =   720
         Width           =   3135
      End
      Begin VB.Label lblbloodgr 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Blood Group :"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1275
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   1560
         Shape           =   4  'Rounded Rectangle
         Top             =   555
         Width           =   3360
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   930
      Left            =   105
      TabIndex        =   0
      Top             =   1680
      Width           =   5115
      Begin VB.CommandButton cmdexit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3720
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Unload the current window"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdclear 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cl&ear"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Clear the sceen"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdsave 
         BackColor       =   &H00E0E0E0&
         Caption         =   "save"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Update the DataBase"
         Top             =   285
         Width           =   1095
      End
      Begin VB.CommandButton cmddelete 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Clear the sceen"
         Top             =   255
         Width           =   1095
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   120
         Shape           =   4  'Rounded Rectangle
         Top             =   165
         Width           =   1335
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   2190
         Shape           =   4  'Rounded Rectangle
         Top             =   150
         Width           =   1335
      End
      Begin VB.Shape Shape7 
         BorderColor     =   &H000080FF&
         Height          =   615
         Left            =   3600
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmbloodgroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim IsMod As Boolean
Dim BloodGroupID As Integer

Private Sub cmdclear_Click()
Call Clear(Me)
'txtbloodgroup.Text = ""
End Sub

Private Sub cmddelete_Click()
If MsgBox("Do You Want To Delete?", vbQuestion + vbYesNo) = vbYes Then
        Dim rs1 As New ADODB.Recordset
            With rs1
                If .State Then .Close
                Dim StrSql As String
                StrSql = "delete * from BloodGroup where PK_BloodGroupID=" & BloodGroupID
                .Open StrSql, cnn, adOpenDynamic, adLockBatchOptimistic
                MsgBox "Record Deleted", vbInformation + vbOKOnly
         
        End With
Else
        Exit Sub
End If
       'DrawAndFillBloodgr
        'Call ItemRefresh
        frmflexbloodgr.DrawAndFillBloodgr
        Unload Me

End Sub
Public Function ItemRefresh()
Dim rs As New ADODB.Recordset
With rs
    If .State Then .Close
       .ActiveConnection = cnn
       .CursorLocation = adUseClient
       .LockType = adLockBatchOptimistic
       .CursorType = adOpenDynamic
       .Open "BloodGroup"
       frmflexbloodgr.DrawAndFillBloodgr
End With
End Function

Private Sub cmdExit_Click()
Unload Me
End Sub

Private Sub cmdsave_Click()
If validatebloodgr = False Then Exit Sub
Dim rsbloodgr As New ADODB.Recordset

If IsMod Then
    rsbloodgr.Open "Select * From BloodGroup Where PK_BloodGroupID=" & BloodGroupID, cnn, adOpenDynamic, adLockOptimistic
    rsbloodgr.Update
    
Else
    rsbloodgr.Open "Select * From BloodGroup Where PK_BloodGroupID=" & BloodGroupID, cnn, adOpenDynamic, adLockOptimistic
    rsbloodgr.AddNew
    
End If

    rsbloodgr("BloodGroupName") = txtbloodgroup.Text
    rsbloodgr.Update

MsgBox "Record save successfully", vbInformation
frmflexbloodgr.DrawAndFillBloodgr
Unload Me
End Sub

Public Function validatebloodgr() As Boolean
If Trim(txtbloodgroup.Text) = "" Then
MsgBox "Enter the Blood Group", vbInformation
txtbloodgroup.SetFocus
validatebloodgr = False
Exit Function
End If
validatebloodgr = True
End Function

Private Sub Command1_Click()

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
IsMod = False
End Sub

Private Sub txtbloodgroup_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 65 To 90
Case 8, 43, 45
Case 32
Case Else
KeyAscii = 0
End Select

End Sub

Public Function AssignBloodGr(bloodgrid As Integer)
Dim rsblood As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From BloodGroup Where PK_BloodGroupID=" & bloodgrid
'If rsblood.State Then rsblood.Close
rsblood.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rsblood.RecordCount > 0 Then
    IsMod = True
    BloodGroupID = bloodgrid
    
    txtbloodgroup.Text = rsblood("BloodGroupName")
End If
End Function
