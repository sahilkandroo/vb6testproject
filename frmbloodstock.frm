VERSION 5.00
Begin VB.Form frmbloodstock 
   BackColor       =   &H00C0FFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Blood Stock"
   ClientHeight    =   2160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3825
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   3825
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   2.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
      Begin VB.ComboBox cbonameid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   6
         ToolTipText     =   "Select your bloodgroup"
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox txtbloodgroup 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1440
         MaxLength       =   10
         TabIndex        =   4
         ToolTipText     =   "Enter Blood Group"
         Top             =   1320
         Width           =   1935
      End
      Begin VB.ComboBox cbobloodgroupname 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         TabIndex        =   3
         ToolTipText     =   "Select your bloodgroup"
         Top             =   240
         Width           =   1935
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H000080FF&
         Height          =   585
         Left            =   1320
         Shape           =   4  'Rounded Rectangle
         Top             =   1200
         Width           =   2175
      End
      Begin VB.Label Label3 
         BackColor       =   &H000080FF&
         Height          =   120
         Left            =   0
         TabIndex        =   5
         Top             =   960
         Width           =   3660
      End
      Begin VB.Shape Shape8 
         BorderColor     =   &H000080FF&
         Height          =   585
         Left            =   1320
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   2175
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "    Stock :"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "BloodGroup"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000080FF&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmbloodstock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Function FillBloodgroupname()
Dim rsbloodgroup As New ADODB.Recordset
With rsbloodgroup
    If rsbloodgroup.State Then rsbloodgroup.Close
    .Open "BloodGroup", cnn, adOpenDynamic, adLockOptimistic
    Dim i As Integer
    For i = 1 To .RecordCount
    cbobloodgroupname.AddItem .Fields("BloodGroupName")
    cbonameid.AddItem .Fields("PK_BloodGroupID")
    .MoveNext
    Next
End With
End Function

Private Sub cbobloodgroupname_Click()
cbonameid.ListIndex = cbobloodgroupname.ListIndex
If cbobloodgroupname.ListIndex = 0 Then
    Call CountAPOSITIVE
End If

End Sub

Private Sub Form_Load()
Call FillBloodgroupname
End Sub

Public Function CountAPOSITIVE()
Dim ctr As Integer
Dim i, a, c As Integer
Dim did As Integer
did = Val(cbonameid.Text)
Dim rsCheck As New ADODB.Recordset
'If rsCheck.Open Then rsCheck.Close
rsCheck.Open "Select * From IndividualMaster", cnn, adOpenDynamic, adLockOptimistic
i = 1
c = 0
If rsCheck.RecordCount > 0 Then
    For ctr = 1 To rsCheck.RecordCount
        a = 0
        'cbodonid.ListIndex = i
        a = Val(frminventorylist.msfallbloodgrlist.TextMatrix(i, 1))
        If did = a Then
            c = c + 1
        End If
        i = i + 1
    Next
End If
    
    txtbloodgroup.Text = Val(c)
End Function
  
            
              
