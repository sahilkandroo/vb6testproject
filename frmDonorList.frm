VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDonorList 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Donor List Report"
   ClientHeight    =   1560
   ClientLeft      =   5205
   ClientTop       =   4725
   ClientWidth     =   5145
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   5145
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Width           =   5055
      Begin VB.CommandButton cmdPrint 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Print"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   570
         Left            =   1920
         Shape           =   4  'Rounded Rectangle
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5055
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   375
         Left            =   3240
         TabIndex        =   2
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   20643843
         CurrentDate     =   39091
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   960
         TabIndex        =   1
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   20643843
         CurrentDate     =   39091
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   2760
         TabIndex        =   4
         Top             =   120
         Width           =   270
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FEEADA&
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   525
      End
   End
End
Attribute VB_Name = "frmDonorList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
Unload Me
End Sub

Private Sub cmdPrint_Click()
If DTPicker1.Value > DTPicker2.Value Then
MsgBox "Please check Selected Dates", vbInformation + vbOKOnly
Exit Sub
End If
If DTPicker2.Value > Now Then
MsgBox "select 2nd Date as Todays Date"
Exit Sub
End If
DonorReport
End Sub


Function DonorReport()
Dim rs As New ADODB.Recordset
Dim StrSql As String
StrSql = " SELECT IndividualMaster.PK_DonorID, IndividualMaster.DonorName, BloodGroup.BloodGroupName, IndividualMaster.Date" & _
         " FROM BloodGroup INNER JOIN IndividualMaster ON BloodGroup.PK_BloodGroupID=IndividualMaster.FK_BloodGroupID" & _
         " WHERE (((IndividualMaster.Date) Between #" & Format(DTPicker1.Value, "dd - MMM - yyyy") & "# And #" & Format(DTPicker2.Value, "dd-MMM-yyyy") & "#))"

    
 
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
If rs.RecordCount < 1 Then
    MsgBox "Record not present", vbInformation
    Exit Function
Else

Set DonorFromTo.DataSource = rs


With DonorFromTo
    .Sections("Section2").Controls.Item("lblFrom").Caption = Format(DTPicker1.Value, "dd-MMM-yyyy")
    .Sections("section2").Controls.Item("lblTo").Caption = Format(DTPicker2.Value, "dd-MMM-yyyy")
End With


With DonorFromTo.Sections("Section1").Controls
    .Item("txtDate").DataField = "Date"
    .Item("txtDonorName").DataField = "DonorName"
    .Item("txtBloodGroup").DataField = "BloodGroupName"
    .Item("txtId").DataField = "PK_DonorID"

    
End With


DonorFromTo.Refresh
DonorFromTo.Show
End If
End Function


Private Sub Form_Load()
DTPicker2.Value = Now
End Sub
