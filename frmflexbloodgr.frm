VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmflexbloodgr 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "BloodGroup_List"
   ClientHeight    =   5430
   ClientLeft      =   4485
   ClientTop       =   3165
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   7200
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid msfbloodgrlist 
      CausesValidation=   0   'False
      Height          =   4485
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   6060
      _ExtentX        =   10689
      _ExtentY        =   7911
      _Version        =   393216
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   14737632
      BackColorSel    =   16706266
      ForeColorSel    =   255
      BackColorBkg    =   16777215
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      ScrollBars      =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmflexbloodgr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdnew_Click()
frmbloodgroup.Show vbModal
frmbloodgroup.cmddelete.Visible = False
End Sub

Private Sub Form_Load()
Call DrawAndFillBloodgr
End Sub
Public Function DrawAndFillBloodgr()
Dim ctr As Integer
Dim rsbloodgr As New ADODB.Recordset

    If rsbloodgr.State Then rsbloodgr.Close
    rsbloodgr.Open "BloodGroup", cnn, adOpenDynamic, adLockOptimistic
    
    With msfbloodgrlist
        .Clear
        .Cols = 3
        .ColWidth(0) = 1
        .ColWidth(1) = 3000
        .ColWidth(2) = 3000
        
        .TextMatrix(0, 0) = "BloodGroupID"
        .TextMatrix(0, 1) = "BloodGroup"
        .TextMatrix(0, 2) = "Available Qty"
        
        .Rows = rsbloodgr.RecordCount + 1
        
        For ctr = 1 To rsbloodgr.RecordCount
            .TextMatrix(ctr, 0) = rsbloodgr("PK_BloodGroupID")
            .TextMatrix(ctr, 1) = rsbloodgr("BloodGroupName")
            .TextMatrix(ctr, 2) = rsbloodgr("AvailableQty")
            rsbloodgr.MoveNext
        Next
    End With
End Function



Private Sub msfbloodgrlist_DblClick()
If msfbloodgrlist.Row = 0 Then Exit Sub
frmbloodgroup.AssignBloodGr Val(msfbloodgrlist.TextMatrix(msfbloodgrlist.Row, 0))
frmbloodgroup.Show vbModal
frmbloodgroup.cmdclear.Visible = False
End Sub
