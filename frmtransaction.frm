VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmtransaction 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Transaction"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8835
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   8835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   120
      TabIndex        =   17
      Top             =   5880
      Width           =   8640
      Begin VB.CommandButton cmdprint 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Print"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Clear the sceen"
         Top             =   165
         Width           =   1050
      End
      Begin VB.CommandButton cmdsavetrans 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Sa&ve"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Update the DataBase"
         Top             =   165
         Width           =   1170
      End
      Begin VB.CommandButton cmdclr 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Cl&ear"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5895
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Clear the sceen"
         Top             =   158
         Width           =   1050
      End
      Begin VB.CommandButton cmdexit 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xit"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7200
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Unload the current window"
         Top             =   150
         Width           =   1050
      End
      Begin VB.Shape Shape7 
         BorderColor     =   &H000080FF&
         Height          =   510
         Left            =   1680
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1185
      End
      Begin VB.Shape Shape3 
         BorderColor     =   &H000080FF&
         Height          =   510
         Left            =   7125
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1185
      End
      Begin VB.Shape Shape5 
         BorderColor     =   &H000080FF&
         Height          =   510
         Left            =   5835
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1185
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H000080FF&
         Height          =   510
         Left            =   120
         Shape           =   4  'Rounded Rectangle
         Top             =   90
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   5700
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8640
      Begin VB.Frame fmMoreDetails 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1950
         Left            =   1380
         TabIndex        =   36
         Top             =   3240
         Visible         =   0   'False
         Width           =   5640
         Begin VB.TextBox txtouthosp 
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1005
            MaxLength       =   20
            TabIndex        =   7
            Top             =   1080
            Width           =   4320
         End
         Begin VB.TextBox txtOutsiderPhone 
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1005
            MaxLength       =   15
            TabIndex        =   6
            Top             =   735
            Width           =   3000
         End
         Begin VB.CommandButton Command2 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   4650
            Style           =   1  'Graphical
            TabIndex        =   9
            ToolTipText     =   "Update the DataBase"
            Top             =   1620
            Width           =   870
         End
         Begin VB.CommandButton cmdoutsiderdetails 
            BackColor       =   &H00E0E0E0&
            Caption         =   "OK"
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3630
            Style           =   1  'Graphical
            TabIndex        =   8
            ToolTipText     =   "Update the DataBase"
            Top             =   1620
            Width           =   945
         End
         Begin VB.TextBox txtAddress 
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   585
            Left            =   1005
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Top             =   90
            Width           =   4395
         End
         Begin VB.Shape Shape4 
            BorderColor     =   &H000080FF&
            Height          =   1545
            Left            =   0
            Shape           =   4  'Rounded Rectangle
            Top             =   0
            Width           =   5625
         End
         Begin VB.Label Label5 
            Caption         =   "Address"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   105
            TabIndex        =   39
            Top             =   150
            Width           =   810
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Phone"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   105
            TabIndex        =   38
            Top             =   765
            Width           =   600
         End
         Begin VB.Label Label3 
            Caption         =   "Hosiptal"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   75
            TabIndex        =   37
            Top             =   1155
            Width           =   1035
         End
      End
      Begin VB.TextBox txttransno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2055
         MaxLength       =   5
         TabIndex        =   41
         ToolTipText     =   "Enter name"
         Top             =   900
         Width           =   1920
      End
      Begin VB.TextBox txtoutsiderid 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   8280
         TabIndex        =   40
         Top             =   1440
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdMoreDetails 
         BackColor       =   &H00E0E0E0&
         Caption         =   "More Details"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   7170
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Update the DataBase"
         Top             =   1365
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.TextBox txtinc 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   600
         TabIndex        =   35
         Top             =   840
         Visible         =   0   'False
         Width           =   930
      End
      Begin VB.TextBox txttransid 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   270
         MaxLength       =   5
         TabIndex        =   34
         ToolTipText     =   "Enter name"
         Top             =   900
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.CommandButton cmdOkData 
         BackColor       =   &H000080FF&
         Caption         =   "&OK"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6285
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Unload the current window"
         Top             =   2610
         Width           =   630
      End
      Begin VB.TextBox txtNOB 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5370
         MaxLength       =   5
         TabIndex        =   11
         ToolTipText     =   "Enter name"
         Top             =   2625
         Width           =   735
      End
      Begin VB.ComboBox cboHospitalID 
         Appearance      =   0  'Flat
         BackColor       =   &H00C00000&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1425
         TabIndex        =   25
         ToolTipText     =   "Select your bloodgroup"
         Top             =   1440
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.ComboBox cbodonid 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1455
         TabIndex        =   23
         ToolTipText     =   "Select your bloodgroup"
         Top             =   1455
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Frame Frame3 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   225
         TabIndex        =   18
         Top             =   315
         Width           =   6840
         Begin VB.OptionButton optHospital 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Hospital"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4725
            TabIndex        =   22
            Top             =   105
            Width           =   1485
         End
         Begin VB.OptionButton optOutsider 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Outsider"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   2835
            TabIndex        =   21
            Top             =   105
            Width           =   1230
         End
         Begin VB.OptionButton optDonor 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Registered Donor"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   105
            Value           =   -1  'True
            Width           =   2025
         End
      End
      Begin MSFlexGridLib.MSFlexGrid msfhosptrans 
         Height          =   2595
         Left            =   120
         TabIndex        =   30
         Top             =   3030
         Width           =   7365
         _ExtentX        =   12991
         _ExtentY        =   4577
         _Version        =   393216
         Rows            =   10
         Cols            =   3
         FixedCols       =   0
         BackColorSel    =   16777215
         BackColorBkg    =   16777215
         FillStyle       =   1
         ScrollBars      =   2
         AllowUserResizing=   3
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.ComboBox cboBG 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   285
         TabIndex        =   10
         ToolTipText     =   "Select your bloodgroup"
         Top             =   2580
         Width           =   1815
      End
      Begin VB.ComboBox cboBGID 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   26
         Top             =   2595
         Visible         =   0   'False
         Width           =   855
      End
      Begin MSComCtl2.DTPicker dtDate 
         Height          =   330
         Left            =   1380
         TabIndex        =   31
         Top             =   2025
         Width           =   1755
         _ExtentX        =   3096
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   71368707
         CurrentDate     =   39088
      End
      Begin VB.TextBox txtOutsider 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1455
         TabIndex        =   2
         Top             =   1455
         Width           =   5670
      End
      Begin VB.ComboBox cboHospital 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1455
         TabIndex        =   3
         ToolTipText     =   "Select your bloodgroup"
         Top             =   1455
         Width           =   5670
      End
      Begin VB.ComboBox cboDonorName 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1455
         TabIndex        =   1
         ToolTipText     =   "Select your bloodgroup"
         Top             =   1455
         Width           =   5670
      End
      Begin VB.Frame frmamt 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   1.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   3120
         TabIndex        =   42
         Top             =   2040
         Width           =   3735
         Begin VB.TextBox txtamount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1260
            MaxLength       =   5
            TabIndex        =   43
            ToolTipText     =   "Enter name"
            Top             =   30
            Width           =   1920
         End
         Begin VB.Label Label7 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Amount"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   240
            TabIndex        =   44
            Top             =   30
            Width           =   750
         End
         Begin VB.Shape Shape2 
            BorderColor     =   &H000080FF&
            Height          =   375
            Left            =   1200
            Shape           =   4  'Rounded Rectangle
            Top             =   0
            Width           =   2040
         End
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H000080FF&
         Height          =   375
         Left            =   1995
         Shape           =   4  'Rounded Rectangle
         Top             =   870
         Width           =   2040
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Transaction No."
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   195
         TabIndex        =   33
         Top             =   900
         Width           =   1605
      End
      Begin VB.Label Label31 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   " Date"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   210
         TabIndex        =   32
         Top             =   2070
         Width           =   555
      End
      Begin VB.Label Label20 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "No Of Bags"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   4155
         TabIndex        =   29
         Top             =   2655
         Width           =   1185
      End
      Begin VB.Shape Shape22 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   165
         Shape           =   4  'Rounded Rectangle
         Top             =   2490
         Width           =   2040
      End
      Begin VB.Label lblstock 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3195
         TabIndex        =   28
         Top             =   2625
         Width           =   735
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   2325
         TabIndex        =   27
         Top             =   2610
         Width           =   735
      End
      Begin VB.Label lblRecipient 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Donor "
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   255
         TabIndex        =   24
         Top             =   1470
         Width           =   690
      End
      Begin VB.Shape Shape20 
         BorderColor     =   &H000080FF&
         Height          =   465
         Left            =   1395
         Shape           =   4  'Rounded Rectangle
         Top             =   1380
         Width           =   5775
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Select Type Of Recipient"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   225
         TabIndex        =   19
         Top             =   90
         Width           =   1875
      End
   End
End
Attribute VB_Name = "frmtransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TransactionID As Integer
Public transoutid As Integer
Dim ctr As Integer
Dim TOR As Integer
Dim IsMod As Boolean

Private Sub cboBG_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 13
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub cboDonorName_Click()
cbodonid.ListIndex = cboDonorName.ListIndex
End Sub

Private Sub cboDonorName_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 13, 65 To 90, 97 To 122, 8
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub cboHospital_Click()
cboHospitalID.ListIndex = cboHospital.ListIndex
End Sub

Private Sub cboHospital_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 13, 65 To 90, 97 To 122, 8
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub cmdclr_Click()
Call Clear(Me)
lblstock.Caption = ""
msfhosptrans.Clear
Call drawgrid
Call autoid
Call autoinc
txttransno.Text = Val(txttransno.Text) + 1
End Sub

Private Sub cmdexit_Click()
Unload Me
End Sub

Private Sub cmdMoreDetails_Click()
fmMoreDetails.Visible = True
End Sub

Private Sub cmdOkData_Click()
If cboBG.Text = "" Then
    MsgBox "Select BloodGroup", vbInformation
    cboBG.SetFocus
    Exit Sub
End If
If txtNOB.Text = "" Then
    MsgBox "Enter No. of Bags", vbInformation
    Exit Sub
End If
If Val(txtNOB.Text) > Val(lblstock.Caption) Then
    MsgBox "Required Bloodbags are greater than available stock", vbCritical
    'MsgBox "" & Val(txtNOB.Text) & " No. of Bags are not available", vbInformation
    txtNOB.Text = ""
    txtNOB.SetFocus
    Exit Sub
    
Else
    lblstock.Caption = Val(lblstock.Caption) - Val(txtNOB.Text)
End If
     If optDonor.Value = True And ctr > 1 Then
     MsgBox "u can not add more than once"
     Exit Sub
     End If
    
     If optOutsider.Value = True And ctr > 1 Then
     MsgBox "u can not add more than once"
     Exit Sub
     End If
    msfhosptrans.TextMatrix(ctr, 0) = cboBGID.Text
    msfhosptrans.TextMatrix(ctr, 1) = cboBG.Text
    msfhosptrans.TextMatrix(ctr, 2) = txtNOB.Text
    ctr = ctr + 1
  '  End If

txtamount.Text = Val(txtNOB.Text) * 625

End Sub

Public Function savehospital()

Dim rs3 As New ADODB.Recordset
Dim bg3, n As Integer
If rs3.State Then rs3.Close
rs3.Open "Trans", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
'While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs3.AddNew
    
    rs3("TypeOfReciepent") = 3
    rs3("TransactionDate") = Format(dtDate.Value, "dd-MMM-yyyy")
    rs3("FK_RecepientID") = cboHospitalID.Text
    rs3("TransactionNo") = txttransno.Text
    'rs1("Amount") = 0
rs3.Update
'msfhosptrans.Row = msfhosptrans.Row + 1
'Wend

MsgBox "Record save successfully", vbInformation

msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
bg3 = msfhosptrans.TextMatrix(msfhosptrans.Row, 0)
n = msfhosptrans.TextMatrix(msfhosptrans.Row, 2)
If rs3.State Then rs3.Close
rs3.Open "Select * From BloodGroup Where PK_BloodGroupID=" & bg3, cnn, adOpenDynamic, adLockOptimistic
rs3.Update
    rs3("AvailableQty") = Val(rs3("AvailableQty")) - Val(n)
rs3.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend

Call autoid

If rs3.State Then rs3.Close
rs3.Open "Select * From TransactionDetails", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs3.AddNew
    rs3("FK_TransID") = txttransid.Text
    rs3("FK_BloodGroupID") = msfhosptrans.TextMatrix(msfhosptrans.Row, 0)
   rs3("NOB") = msfhosptrans.TextMatrix(msfhosptrans.Row, 2)
rs3.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend
Call Clear(Me)
lblstock.Caption = ""
msfhosptrans.Clear
Unload Me
End Function



Public Function savedonor()
'On Error Resume Next
Dim rs1 As New ADODB.Recordset
Dim bg1 As Integer
If rs1.State Then rs1.Close
rs1.Open "Trans", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs1.AddNew
    
    rs1("TypeOfReciepent") = 1
    rs1("TransactionDate") = Format(dtDate.Value, "dd-MMM-yyyy")
    rs1("FK_RecepientID") = cbodonid.Text
    rs1("TransactionNo") = txttransno.Text
    rs1("Amount") = 0
rs1.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend
bg1 = cboBGID.Text
MsgBox "Record save successfully", vbInformation


Call autoid
    
    
If rs1.State Then rs1.Close
rs1.Open "Select * From BloodGroup Where PK_BloodGroupID=" & bg1, cnn, adOpenDynamic, adLockOptimistic
rs1.Update
    rs1("AvailableQty") = Val(lblstock.Caption) ' - Val(txtNOB.Text)
rs1.Update


If rs1.State Then rs1.Close
rs1.Open "Select * From TransactionDetails", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs1.AddNew
    rs1("FK_TransID") = txttransid.Text
    rs1("FK_BloodGroupID") = cboBGID.Text
    rs1("NOB") = txtNOB.Text
rs1.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend
Call Clear(Me)
lblstock.Caption = ""
msfhosptrans.Clear
End Function
Public Function saveoutsider()

'On Error Resume Next
Dim rs2 As New ADODB.Recordset
Dim bg2 As Integer
If rs2.State Then rs2.Close
rs2.Open "Trans", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs2.AddNew
    
    rs2("TypeOfReciepent") = 2
    rs2("TransactionDate") = Format(dtDate.Value, "dd-MMM-yyyy")
    rs2("FK_RecepientID") = txtoutsiderid.Text
    rs2("TransactionNo") = txttransno.Text
    rs2("Amount") = txtamount.Text
rs2.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend
bg2 = cboBGID.Text
MsgBox "Record save successfully", vbInformation

If rs2.State Then rs2.Close
rs2.Open "Select * From BloodGroup Where PK_BloodGroupID=" & bg2, cnn, adOpenDynamic, adLockOptimistic
rs2.Update
    rs2("AvailableQty") = Val(lblstock.Caption) ' - Val(txtNOB.Text)
rs2.Update

Call autoid
If rs2.State Then rs2.Close
rs2.Open "Select * From TransactionDetails", cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
While msfhosptrans.TextMatrix(msfhosptrans.Row, 0) <> ""
rs2.AddNew
    rs2("FK_TransID") = txttransid.Text
    rs2("FK_BloodGroupID") = cboBGID.Text
    rs2("NOB") = txtNOB.Text
rs2.Update
msfhosptrans.Row = msfhosptrans.Row + 1
Wend

'Call Clear(Me)
'msfhosptrans.Clear
cmdprint.Enabled = True
End Function

Public Function filloutsider()
Dim rso As New ADODB.Recordset
If rso.State Then rso.Close
rso.Open "Select * From OutSider", cnn, adOpenDynamic, adLockOptimistic
    rso.AddNew
        rso("Name") = txtOutsider.Text
'        rso ("")
End Function



Private Sub cmdoutsiderdetails_Click()
If txtAddress.Text = "" Then
    MsgBox "Enter Address", vbInformation
    txtAddress.SetFocus
    Exit Sub
End If

If txtOutsiderPhone.Text = "" Then
    MsgBox "Enter Phone No.", vbInformation
    txtOutsiderPhone.SetFocus
    Exit Sub
End If

If txtouthosp.Text = "" Then
    MsgBox "Enter Hospital Name", vbInformation
    txtouthosp.SetFocus
    Exit Sub
End If

Dim k As Integer
Dim rsoutdet As New ADODB.Recordset
If rsoutdet.State Then rsoutdet.Close
rsoutdet.Open "OutSider", cnn, adOpenDynamic, adLockOptimistic
rsoutdet.AddNew
    rsoutdet("Name") = txtOutsider.Text
    rsoutdet("PhoneNo") = txtOutsiderPhone.Text
    rsoutdet("HospName") = txtouthosp.Text
    rsoutdet("Address") = txtAddress.Text
rsoutdet.Update

If rsoutdet.State Then rsoutdet.Close

rsoutdet.Open "Select PK_OutSiderID From OutSider", cnn, adOpenDynamic, adLockOptimistic
For k = 1 To rsoutdet.RecordCount
    txtoutsiderid.Text = rsoutdet("PK_OutSiderID")
    rsoutdet.MoveNext
Next
fmMoreDetails.Visible = False
End Sub
Private Sub cmdsavetrans_Click()
If optDonor.Value = True Then
    If donor_validation Then
        Call savedonor
        Unload Me
    Else
        Exit Sub
    End If
End If


If optOutsider.Value = True Then
    If out_validation = True Then
        Call saveoutsider
    Else
        Exit Sub
    End If
End If


If optHospital.Value = True Then
    If hosp_valid = True Then
        Call savehospital
        Unload Me
    Else
        Exit Sub
    End If
End If
'Call Clear(Me)

Dim rs As New ADODB.Recordset
Dim a As Integer
If rs.State Then rs.Close
rs.Open "Trans", cnn, adOpenDynamic, adLockOptimistic
For a = 1 To rs.RecordCount
    transoutid = rs("PK_TransactionID")
    rs.MoveNext
Next
'Unload Me
End Sub



Private Sub Command2_Click()
fmMoreDetails.Visible = False
End Sub

Private Sub Form_Load()
FillCombos
If IsMod = False Then
Call autoinc
    txttransno.Text = Val(txttransno.Text) + 1
End If
Call drawgrid
Call optDonor_Click
dtDate.Value = Now
txtamount.Text = 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
IsMod = False
End Sub

Private Sub msfhosptrans_Click()
'MsgBox msfhosptrans.CellWidth
End Sub

Private Sub optDonor_Click()
TOR = 0
lblRecipient.Caption = "Donor"
cboDonorName.Visible = True
cboHospital.Visible = False
txtOutsider.Visible = False
fmMoreDetails.Visible = False
cmdMoreDetails.Visible = False
frmamt.Visible = False
Shape7.Visible = False
cmdprint.Visible = False
msfhosptrans.Clear
Call drawgrid
'Call Clear
cboBG.Clear
cboBGID.Clear
lblstock.Caption = ""
txtNOB.Text = ""
Call FillBloodgroup
ctr = 1


End Sub

Private Sub optHospital_Click()
TOR = 2
lblRecipient.Caption = "Hospital"
cboHospital.Visible = True
cboDonorName.Visible = False
txtOutsider.Visible = False
fmMoreDetails.Visible = False
cmdMoreDetails.Visible = False
frmamt.Visible = False
Shape7.Visible = False
cmdprint.Visible = False
msfhosptrans.Clear
Call drawgrid
cboBG.Clear
cboBGID.Clear
lblstock.Caption = ""
txtNOB.Text = ""
Call FillBloodgroup
ctr = 1
End Sub

Private Sub optOutsider_Click()
TOR = 1
lblRecipient.Caption = "Outsider"
txtOutsider.Visible = True
'------------------added
cmdMoreDetails.Visible = True
'----------------------
cboDonorName.Visible = False
cboHospital.Visible = False
fmMoreDetails.Visible = False
cmdMoreDetails.Visible = True
frmamt.Visible = True
Shape7.Visible = True
cmdprint.Visible = True
msfhosptrans.Clear
Call drawgrid
cboBG.Clear
cboBGID.Clear
lblstock.Caption = ""
txtNOB.Text = ""
Call FillBloodgroup
ctr = 1
End Sub


Sub FillCombos()
FillBloodgroup
Dim rs As New ADODB.Recordset
Dim StrSql As String
StrSql = "Select * From HospitalMaster"
If rs.State Then rs.Close
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
cboHospitalID.Clear
cboHospital.Clear
While Not rs.EOF = True
    cboHospitalID.AddItem rs("PK_HospitalID")
    cboHospital.AddItem rs("HospitalName")
    rs.MoveNext
Wend


StrSql = "Select * From IndividualMaster"
If rs.State Then rs.Close
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
cbodonid.Clear
cboDonorName.Clear
While Not rs.EOF = True
    cbodonid.AddItem rs("PK_DonorID")
    cboDonorName.AddItem rs("DonorName")
    rs.MoveNext
Wend
End Sub

Public Function FillBloodgroup()
Dim rsbloodgroup1 As New ADODB.Recordset
With rsbloodgroup1
    If rsbloodgroup1.State Then rsbloodgroup1.Close
    .Open "BloodGroup", cnn, adOpenDynamic, adLockOptimistic
    Dim i As Integer
    For i = 1 To .RecordCount
    cboBG.AddItem .Fields("BloodGroupName")
    cboBGID.AddItem .Fields("PK_BloodGroupID")
    .MoveNext
    Next
End With
End Function
Private Sub cboBG_Click()
cboBGID.ListIndex = cboBG.ListIndex
Dim rsstock As New ADODB.Recordset
If rsstock.State Then rsstock.Close
Dim strsqlstock As String
strsqlstock = "Select AvailableQty From BloodGroup Where PK_BloodGroupID=" & Val(cboBGID.Text)
rsstock.Open strsqlstock, cnn, adOpenDynamic, adLockOptimistic
    lblstock.Caption = rsstock("AvailableQty")
End Sub
Public Function drawgrid()
    msfhosptrans.TextMatrix(0, 0) = "BloodGroupID"
    msfhosptrans.TextMatrix(0, 1) = "BloodGroup"
    msfhosptrans.TextMatrix(0, 2) = "No OF Bags"
    msfhosptrans.ColWidth(0) = 1
    msfhosptrans.ColWidth(1) = 3682
    msfhosptrans.ColWidth(2) = 3592
End Function
  
Public Function autoinc()
Dim rs9 As New ADODB.Recordset
Dim strsqlinc As String
Dim u As Integer
If rs9.State Then rs9.Close
strsqlinc = "Select * From Trans"
rs9.Open strsqlinc, cnn, adOpenDynamic, adLockOptimistic
If rs9.RecordCount > 0 Then
For u = 1 To rs9.RecordCount
txttransno.Text = rs9("TransactionNo")
rs9.MoveNext
Next
Else
txttransno.Text = 0
End If
End Function

Public Function autoid()
Dim rs7 As New ADODB.Recordset
Dim strsqlinc As String
Dim u As Integer
If rs7.State Then rs7.Close
strsqlinc = "Select * From Trans"
rs7.Open strsqlinc, cnn, adOpenDynamic, adLockOptimistic
If rs7.RecordCount > 0 Then
For u = 1 To rs7.RecordCount
txttransid.Text = rs7("PK_TransactionID")
rs7.MoveNext
Next
Else
txttransid.Text = 0
End If

End Function

Public Function TransData(transid As Integer)
Dim StrSql As String
Dim rs As New ADODB.Recordset
Dim t As Integer
If rs.State Then rs.Close
StrSql = "Select * From Trans Where PK_TransactionID=" & transid
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
    TransactionID = transid
    dtDate.Value = rs("TransactionDate")
    transoutid = rs("PK_TransactionID")
    txttransno.Text = rs("TransactionNo")
    txtamount.Text = rs("Amount")
''''''''''''''''''''''''''''''''''''''''''''


StrSql = "SELECT TransactionDetails.NOB, TransactionDetails.FK_TransID, TransactionDetails.FK_BloodGroupID, BloodGroup.BloodGroupName" & _
         " FROM (Trans INNER JOIN TransactionDetails ON Trans.PK_TransactionID = TransactionDetails.FK_TransID) INNER JOIN BloodGroup ON TransactionDetails.FK_BloodGroupID = BloodGroup.PK_BloodGroupID" & _
         " WHERE (((TransactionDetails.FK_TransID)=" & transid & "))"


msfhosptrans.Clear
If rs.State Then rs.Close
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
msfhosptrans.Row = 1
For t = 1 To rs.RecordCount
    msfhosptrans.TextMatrix(msfhosptrans.Row, 0) = rs("FK_BloodGroupID")
    msfhosptrans.TextMatrix(msfhosptrans.Row, 1) = rs("BloodGroupName")
    msfhosptrans.TextMatrix(msfhosptrans.Row, 2) = rs("NOB")
    msfhosptrans.Row = msfhosptrans.Row + 1
    rs.MoveNext
Next
End Function


Private Sub cmdPrint_Click()
OutsiderTransReport
End Sub


Function OutsiderTransReport()
Dim rate As Integer
rate = 625
Dim rs As New ADODB.Recordset
If rs.State Then rs.Close
Dim StrSql As String
StrSql = "SELECT Trans.PK_TransactionID, TransactionDetails.NOB, Trans.Amount, BloodGroup.BloodGroupName, BloodGroup.Rate" & _
        " FROM (Trans INNER JOIN TransactionDetails ON Trans.PK_TransactionID = TransactionDetails.FK_TransID) INNER JOIN BloodGroup ON TransactionDetails.FK_BloodGroupID = BloodGroup.PK_BloodGroupID" & _
        " WHERE (((Trans.PK_TransactionID)=" & transoutid & "))"
rs.Open StrSql, cnn, adOpenDynamic, adLockOptimistic
Set OusiderBill.DataSource = rs

With OusiderBill
    .Sections("Section2").Controls.Item("lblBillNo").Caption = txttransno.Text
    .Sections("section2").Controls.Item("lblDate").Caption = Format(dtDate.Value, "dd-MMM-yyyy")
    .Sections("section2").Controls.Item("lblName").Caption = txtOutsider.Text
End With


With OusiderBill.Sections("Section1").Controls
    .Item("txtBloodGroup").DataField = "BloodGroupName"
    .Item("txtNOB").DataField = "NOB"
    .Item("txtRate").DataField = "Rate"
    .Item("txtAmount").DataField = "Amount"
End With



With OusiderBill.Sections("Section5").Controls
    .Item("funtotal").DataField = "Amount"
End With

OusiderBill.Refresh
OusiderBill.Show
End Function


Function donor_validation() As Boolean

If Trim(cboDonorName.Text) = "" Then
    MsgBox "Select Donor Name ", vbInformation
    cboDonorName.SetFocus
    donor_validation = False
    Exit Function
End If

msfhosptrans.Row = 1
If msfhosptrans.TextMatrix(msfhosptrans.Row, 0) = "" Then
    MsgBox "Select your Blood Requirement"
    cboBG.SetFocus
    donor_validation = False
    Exit Function
End If
donor_validation = True
End Function

Function out_validation() As Boolean


If Trim(txtOutsider.Text) = "" Then
    MsgBox "Enter Outsider Recipient Name ", vbInformation
    txtOutsider.SetFocus
    out_validation = False
    Exit Function
End If

If Trim(txtAddress.Text) = "" Then
    MsgBox "Enter Outsider-Recipient Address ", vbInformation
    fmMoreDetails.Visible = True
    txtAddress.SetFocus
    out_validation = False
    Exit Function
End If

If Trim(txtOutsiderPhone.Text) = "" Then
    MsgBox "Enter Outsider-Recipient Phone No. ", vbInformation
    fmMoreDetails.Visible = True
    txtOutsiderPhone.SetFocus
    out_validation = False
    Exit Function
End If


If Trim(txtouthosp.Text) = "" Then
    MsgBox "Enter Outsider-Recipient Hospital Name ", vbInformation
    fmMoreDetails.Visible = True
    txtouthosp.SetFocus
    out_validation = False
    Exit Function
End If


msfhosptrans.Row = 1
If msfhosptrans.TextMatrix(msfhosptrans.Row, 0) = "" Then
    MsgBox "Select your Blood Requirement"
    cboBG.SetFocus
    out_validation = False
    Exit Function
End If
out_validation = True

End Function

Function hosp_valid() As Boolean

If Trim(cboHospital.Text) = "" Then
    MsgBox "Select Hospital Name ", vbInformation
    cboHospital.SetFocus
    hosp_valid = False
    Exit Function
End If

msfhosptrans.Row = 1
If msfhosptrans.TextMatrix(msfhosptrans.Row, 0) = "" Then
    MsgBox "Select your Blood Requirement"
    cboBG.SetFocus
    hosp_valid = False
    Exit Function
End If
hosp_valid = True

End Function


Public Function automemb()
Dim rs9 As New ADODB.Recordset
Dim strsqlmemb As String
Dim u As Integer
If rs9.State Then rs9.Close
strsqlmemb = "Select TransactionNo From Trans"
rs9.Open strsqlmemb, cnn, adOpenDynamic, adLockOptimistic
If rs9.RecordCount > 0 Then
For u = 1 To rs9.RecordCount
txtinc.Text = rs9("TransactionNo")
rs9.MoveNext
Next
Else
txtinc.Text = 0
End If
End Function


Private Sub txtAddress_KeyPress(KeyAscii As Integer)
'MsgBox KeyAscii
Select Case KeyAscii
Case 8
Case 32
Case 65 To 90, 97 To 122, 44 To 57, 40, 41
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtamount_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 13
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtNOB_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtouthosp_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32, 46
Case 65 To 90
Case 97 To 122
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtOutsider_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8
Case 32, 46
Case 65 To 90
Case 97 To 122
Case Else
KeyAscii = 0
End Select

End Sub

Private Sub txtOutsiderPhone_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
Case 8, 45
Case 48 To 57
Case Else
KeyAscii = 0
End Select

End Sub
