VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmhosptrans 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hospital Transaction"
   ClientHeight    =   1890
   ClientLeft      =   4890
   ClientTop       =   4425
   ClientWidth     =   5595
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1890
   ScaleWidth      =   5595
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5535
      Begin VB.ComboBox cboHospitalName 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   405
         Left            =   1800
         TabIndex        =   2
         Text            =   "Select Hospital Name"
         Top             =   120
         Width           =   3615
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   3720
         TabIndex        =   5
         Top             =   600
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   73400323
         CurrentDate     =   39089
      End
      Begin VB.ComboBox cbohospid 
         Height          =   315
         Left            =   1080
         TabIndex        =   4
         Text            =   "Combo2"
         Top             =   600
         Visible         =   0   'False
         Width           =   1590
      End
      Begin VB.Label lblFrom 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   3000
         TabIndex        =   6
         Top             =   600
         Width           =   495
      End
      Begin VB.Label lblCustName 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hospital Name:"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   1575
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   1.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   1200
      Width           =   5535
      Begin VB.CommandButton cmdprint 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Print"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   120
         Width           =   975
      End
      Begin VB.Shape Shape6 
         BorderColor     =   &H000080FF&
         Height          =   495
         Left            =   2160
         Shape           =   4  'Rounded Rectangle
         Top             =   60
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmhosptrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cboHospitalName_Click()
cbohospid.ListIndex = cboHospitalName.ListIndex
End Sub

Private Sub cmdPrint_Click()
If cbohospid.ListIndex >= 0 Then
    HospitalTrans
Else
    MsgBox "Select Hospital Name First", vbInformation
    Exit Sub
End If
End Sub
Function HospitalTrans()
Dim rs As New ADODB.Recordset
Dim Strsql1 As String
'Strsql1 = " SELECT TransactionDetails.NOB, BloodGroup.BloodGroupName, Trans.TypeOfReciepent, Trans.TransactionDate, Trans.FK_RecepientID" & _
'          " FROM HospitalMaster INNER JOIN ((Trans INNER JOIN TransactionDetails ON Trans.PK_TransactionID = TransactionDetails.FK_TransID) INNER JOIN BloodGroup ON TransactionDetails.FK_BloodGroupID = BloodGroup.PK_BloodGroupID) ON HospitalMaster.PK_HospitalID = Trans.FK_RecepientID" & _
'          " WHERE (((Trans.TypeOfReciepent)='3') AND ((Trans.TransactionDate) = # " & DTPicker1.Value & " #) AND ((TransactionDetails.FK_RecepientID)=' " & cbohospid.Text & " '))"


Strsql1 = "SELECT HospitalMaster.HospitalName, Trans.TransactionDate, TransactionDetails.NOB, BloodGroup.BloodGroupName " & _
          " FROM (HospitalMaster INNER JOIN (Trans INNER JOIN TransactionDetails ON Trans.PK_TransactionID = TransactionDetails.FK_TransID) ON HospitalMaster.PK_HospitalID = Trans.FK_RecepientID) INNER JOIN BloodGroup ON TransactionDetails.FK_BloodGroupID = BloodGroup.PK_BloodGroupID " & _
          " GROUP BY HospitalMaster.HospitalName, Trans.TransactionDate, TransactionDetails.NOB, BloodGroup.BloodGroupName, Trans.PK_TransactionID " & _
          " HAVING (((Trans.PK_TransactionID)=(SELECT Trans.PK_TransactionID FROM Trans " & _
          " WHERE (((Trans.TypeOfReciepent)='3') AND ((Trans.FK_RecepientID)=" & Val(cbohospid.Text) & ")  " & _
          " AND ((Trans.TransactionDate)=#" & DTPicker1.Value & "#)))))"

rs.Open Strsql1, cnn, adOpenDynamic, adLockOptimistic
If rs.RecordCount < 1 Then
    MsgBox "Record not present", vbInformation
    Exit Function
Else
Set HospitalTransaction.DataSource = rs
With HospitalTransaction
    .Sections("Section2").Controls.Item("lblDate1").Caption = Format(DTPicker1.Value, "dd-MMM-yyyy")
    .Sections("Section2").Controls.Item("lblHospName").Caption = cboHospitalName.Text
End With
With HospitalTransaction.Sections("Section1").Controls
    .Item("txtBloodGroup").DataField = "BloodGroupName"
    .Item("txtNOB").DataField = "NOB"
End With

HospitalTransaction.Refresh
HospitalTransaction.Show vbModal
End If
End Function
Private Sub Form_Load()
Call fillcombo
End Sub
Public Function fillcombo()
Dim rs1 As New ADODB.Recordset
Dim i As Integer
Dim Strs1ql As String
Strs1ql = "SELECT distinct HospitalMaster.HospitalName,Trans.TypeOfReciepent, Trans.FK_RecepientID" & _
        " FROM Trans INNER JOIN HospitalMaster ON Trans.FK_RecepientID = HospitalMaster.PK_HospitalID" & _
        " WHERE (((Trans.TypeOfReciepent)='3'))"
If rs1.State Then rs1.Close
rs1.Open Strs1ql, cnn, adOpenDynamic, adLockOptimistic
    For i = 1 To rs1.RecordCount
        cboHospitalName.AddItem rs1.Fields("HospitalName")
       cbohospid.AddItem rs1.Fields("FK_RecepientID")
    rs1.MoveNext
    Next
End Function


