VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmflexdesclist 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Address Book List"
   ClientHeight    =   7350
   ClientLeft      =   2070
   ClientTop       =   2565
   ClientWidth     =   11370
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7350
   ScaleWidth      =   11370
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1560
      TabIndex        =   2
      Top             =   240
      Width           =   5745
   End
   Begin VB.CommandButton cmdnew 
      BackColor       =   &H00E0E0E0&
      Caption         =   "N&ew"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Enter new BloodGroup"
      Top             =   240
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid msfdesclist 
      Height          =   5205
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   9181
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      BackColorSel    =   16706266
      ForeColorSel    =   255
      BackColorBkg    =   16777215
      AllowBigSelection=   0   'False
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmflexdesclist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdnew_Click()
frmdescription.Show vbModal
frmdescription.cmddelete.Visible = False
frmdescription.cmdclear.Visible = True
End Sub

Private Sub Form_Load()
Call DrawAndFillDesc
End Sub
Public Function DrawAndFillDesc()
Dim ctr As Integer
Dim rsdesc As New ADODB.Recordset

    If rsdesc.State Then rsdesc.Close
    rsdesc.Open "TypeOfContact", cnn, adOpenDynamic, adLockOptimistic
    
    With msfdesclist
        .Clear
        .ColWidth(0) = 1
        .ColWidth(1) = 6500
        .ColWidth(2) = 4000
        
        .TextMatrix(0, 0) = "PK_TOCID"
        .TextMatrix(0, 1) = "Name"
        .TextMatrix(0, 2) = "Phone No"
        
        .Rows = rsdesc.RecordCount + 1
        
        For ctr = 1 To rsdesc.RecordCount
            .TextMatrix(ctr, 0) = rsdesc("PK_TOCID")
            .TextMatrix(ctr, 1) = rsdesc("Name")
            .TextMatrix(ctr, 2) = rsdesc("PhoneNo")
            
            rsdesc.MoveNext
        Next
    End With
End Function



Private Sub msfdesclist_DblClick()
If msfdesclist.Row = 0 Then Exit Sub
frmdescription.Assigndesc Val(msfdesclist.TextMatrix(msfdesclist.Row, 0))
frmdescription.Show vbModal
frmdescription.cmddelete.Visible = True
frmdescription.cmdclear.Visible = False
End Sub

Private Sub txtSearch_Change()

Dim i As Integer
For i = 1 To msfdesclist.Rows - 1
    If InStr(1, Mid(msfdesclist.TextMatrix(i, 1), 1, Len(txtSearch.Text)), txtSearch.Text, vbBinaryCompare) > 0 Then
        msfdesclist.Row = i
        msfdesclist.RowSel = i
        msfdesclist.ColSel = 2
        'msfindividuallist.SetFocus
        'HighLightRow i
        Exit For
    End If
Next


End Sub
